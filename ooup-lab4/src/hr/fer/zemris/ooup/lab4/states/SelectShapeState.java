package hr.fer.zemris.ooup.lab4.states;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.ooup.lab4.drawing.DocumentModel;
import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.model.CompositeShape;
import hr.fer.zemris.ooup.lab4.model.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.model.Rectangle;

public class SelectShapeState extends IdleState {

	private final static int BOUNDING_BOX_WIDTH = 3;

	private GraphicalObject selectedObject;
	private DocumentModel model;
	private int indexOfSelectedHotPoint;

	public SelectShapeState(DocumentModel model) {
		this.model = model;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		GraphicalObject object = model.findSelectedGraphicalObject(mousePoint);
		if (object == null) {
			selectedObject = null;
			model.deselectAll();
			return;
		}

		if (!ctrlDown) {
			model.deselectAll();
			object.setSelected(true);
			selectedObject = object;
			object.rotate();

		} else {
			selectedObject = null;
			if (!object.isSelected()) {
				object.setSelected(true);
				object.rotate();
			} else {
				object.setSelected(false);
				object.rotate();
			}
		}
	}

	@Override
	public void keyPressed(int keyCode) {
		switch (keyCode) {
		case KeyEvent.VK_LEFT:
			model.getSelectedObjects().forEach(o -> o.translate(new Point(-1, 0)));
			break;

		case KeyEvent.VK_RIGHT:
			model.getSelectedObjects().forEach(o -> o.translate(new Point(1, 0)));
			break;

		case KeyEvent.VK_UP:
			model.getSelectedObjects().forEach(o -> o.translate(new Point(0, -1)));
			break;

		case KeyEvent.VK_DOWN:
			model.getSelectedObjects().forEach(o -> o.translate(new Point(0, 1)));

		case KeyEvent.VK_ADD:// numpad plus "+"
		case KeyEvent.VK_PLUS:
			List<GraphicalObject> selectedObjectsPlus = model.getSelectedObjects();
			if (!selectedObjectsPlus.isEmpty() && selectedObjectsPlus.size() == 1) {
				model.increaseZ(selectedObjectsPlus.get(0));
			}
			break;

		case KeyEvent.VK_SUBTRACT:// numpad minus "-"
		case KeyEvent.VK_MINUS:
			List<GraphicalObject> selectedObjectsMinus = model.getSelectedObjects();
			if (!selectedObjectsMinus.isEmpty() && selectedObjectsMinus.size() == 1) {
				model.decreaseZ(selectedObjectsMinus.get(0));
			}
			break;

		case KeyEvent.VK_G:
			List<GraphicalObject> selectedObjects = model.getSelectedObjects();
			if (!selectedObjects.isEmpty() && selectedObjects.size() > 1) {
				GraphicalObject[] compositeObjects = new GraphicalObject[selectedObjects.size()];
				for (int i = 0; i < compositeObjects.length; i++) {
					compositeObjects[i] = selectedObjects.get(0);
					model.removeGraphicalObject(selectedObjects.get(0));
				}
				GraphicalObject go = new CompositeShape(false, compositeObjects);
				model.addGraphicalObject(go);
				go.setSelected(true);
			}
			break;

		case KeyEvent.VK_U:
			List<GraphicalObject> selectedObjectsUngroup = model.getSelectedObjects();
			if (!selectedObjectsUngroup.isEmpty() && selectedObjectsUngroup.size() == 1
					&& selectedObjectsUngroup.get(0).getShapeName().equals("Composite")) {
				CompositeShape composite = (CompositeShape) selectedObjectsUngroup.get(0);
				List<GraphicalObject> objects = composite.getObjects();
				model.removeGraphicalObject(composite);
				for (GraphicalObject go : objects) {
					go.setSelected(true);
					model.addGraphicalObject(go);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void mouseDragged(Point mousePoint) {
		if (selectedObject != null && indexOfSelectedHotPoint >= 0) {
			if (selectedObject.isHotPointSelected(indexOfSelectedHotPoint)) {
				selectedObject.setHotPoint(indexOfSelectedHotPoint, mousePoint);
			}
		} else {
			if (selectedObject != null) {
				int index = model.findSelectedHotPoint(selectedObject, mousePoint);
				if (index != -1) {
					selectedObject.setHotPointSelected(index, true);
					indexOfSelectedHotPoint = index;
					selectedObject.setHotPoint(index, mousePoint);
				}
			}
		}
	}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		if (selectedObject != null && indexOfSelectedHotPoint >= 0
				&& indexOfSelectedHotPoint < selectedObject.getNumberOfHotPoints()) {
			selectedObject.setHotPointSelected(indexOfSelectedHotPoint, false);
			indexOfSelectedHotPoint = -1;
		}
	}

	@Override
	public void afterDraw(Renderer r, GraphicalObject go) {
		if (!go.isSelected())
			return;
		Rectangle rect = go.getBoundingBox();
		// drawing bounding box
		r.drawLine(new Point(rect.getX(), rect.getY()), new Point((int) rect.getMaxX(), rect.getY()));// upper
		// line
		r.drawLine(new Point(rect.getX(), rect.getY()), new Point(rect.getX(), (int) rect.getMaxY()));// left
		// line
		r.drawLine(new Point(rect.getX(), (int) rect.getMaxY()), new Point((int) rect.getMaxX(), (int) rect.getMaxY()));// bottom
		// line
		r.drawLine(new Point((int) rect.getMaxX(), rect.getY()), new Point((int) rect.getMaxX(), (int) rect.getMaxY()));// right
		// line

		if (selectedObject != null && selectedObject.equals(go)) {
			// draw hot points
			List<Point> hotPoints = new ArrayList<Point>();
			for (int i = 0; i < go.getNumberOfHotPoints(); i++) {
				hotPoints.add(go.getHotPoint(i));
			}
			for (Point point : hotPoints) {
				r.drawLine(new Point(point.getX() - BOUNDING_BOX_WIDTH, point.getY() - BOUNDING_BOX_WIDTH),
						new Point(point.getX() + BOUNDING_BOX_WIDTH, point.getY() - BOUNDING_BOX_WIDTH));// upper
				// line
				r.drawLine(new Point(point.getX() - BOUNDING_BOX_WIDTH, point.getY() - BOUNDING_BOX_WIDTH),
						new Point(point.getX() - BOUNDING_BOX_WIDTH, point.getY() + BOUNDING_BOX_WIDTH));// left
				// line
				r.drawLine(new Point(point.getX() - BOUNDING_BOX_WIDTH, point.getY() + BOUNDING_BOX_WIDTH),
						new Point(point.getX() + BOUNDING_BOX_WIDTH, point.getY() + BOUNDING_BOX_WIDTH));// bottom
				// line
				r.drawLine(new Point(point.getX() + BOUNDING_BOX_WIDTH, point.getY() - BOUNDING_BOX_WIDTH),
						new Point(point.getX() + BOUNDING_BOX_WIDTH, point.getY() + BOUNDING_BOX_WIDTH));// right
				// line
			}
		}
	}

	@Override
	public void onLeaving() {
		selectedObject = null;
		model.deselectAll();
	}
}
