package hr.fer.zemris.ooup.lab4.states;

import hr.fer.zemris.ooup.lab4.drawing.DocumentModel;
import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.model.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;

public class AddShapeState extends IdleState {

	private GraphicalObject prototype;
	private DocumentModel model;

	public AddShapeState(DocumentModel model, GraphicalObject prototype) {
		this.model = model;
		this.prototype = prototype;
	}

	@Override
	public void mouseDown(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		GraphicalObject clone = prototype.duplicate();
		clone.translate(mousePoint);
		model.addGraphicalObject(clone);
	}

}
