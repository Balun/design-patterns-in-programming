package hr.fer.zemris.ooup.lab4.states;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import hr.fer.zemris.ooup.lab4.drawing.DocumentModel;
import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.model.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;

public class EraserState extends IdleState {

	private DocumentModel model;
	private List<Point> points;

	public EraserState(DocumentModel model) {
		this.model = model;
		this.points = new ArrayList<>();
	}

	@Override
	public void mouseDragged(Point mousePoint) {
		points.add(mousePoint);
		model.notifyListeners();
	}

	@Override
	public void mouseUp(Point mousePoint, boolean shiftDown, boolean ctrlDown) {
		points.add(mousePoint);

		Set<GraphicalObject> objectsToDelete = new HashSet<GraphicalObject>();
		for (Point point : points) {
			objectsToDelete.addAll(model.findSelectedGraphicalObjects(point));
		}
		for (GraphicalObject go : objectsToDelete) {
			model.removeGraphicalObject(go);
		}
		points.clear();
		model.notifyListeners();

	}

	@Override
	public void afterDraw(Renderer r) {
		for (int i = 0; i < points.size() - 1; i++) {
			r.drawLine(points.get(i), points.get(i + 1));
		}
	}
}
