package hr.fer.zemris.ooup.lab4.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;

public class IconProvider {

	public static ImageIcon getIcon(String name) {
		String path = "res/" + name + ".png";

		InputStream input;
		try {
			input = new FileInputStream(path);

			byte[] bytes = new byte[4096];
			try (InputStream is = new BufferedInputStream(input)) {
				is.read(bytes);
			} catch (IOException e1) {
				System.err.println("Unable to read icon..");
			} finally {
				try {
					input.close();
				} catch (IOException e) {
				}
			}

			return new ImageIcon(bytes);

		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}

		return null;
	}

}
