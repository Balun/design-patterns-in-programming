package hr.fer.zemris.ooup.lab4.util;

import java.awt.geom.Line2D;

import hr.fer.zemris.ooup.lab4.model.Point;

public class GeometryUtil {

	private GeometryUtil() {
	}

	public static double distanceFromPoint(Point point1, Point point2) {
		return Math.sqrt(Math.pow(point2.getX() - point1.getX(), 2) + Math.pow(point2.getY() - point1.getY(), 2));
	}

	public static double distanceFromLineSegment(Point s, Point e, Point p) {
		return Line2D.ptSegDist(s.getX(), s.getY(), e.getX(), e.getY(), p.getX(), p.getY());
	}
}
