package hr.fer.zemris.ooup.lab4;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import hr.fer.zemris.ooup.lab4.drawing.DocumentModel;
import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.drawing.SVGRendererImpl;
import hr.fer.zemris.ooup.lab4.gui.DrawingCanvas;
import hr.fer.zemris.ooup.lab4.model.CompositeShape;
import hr.fer.zemris.ooup.lab4.model.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.LineSegment;
import hr.fer.zemris.ooup.lab4.model.Oval;
import hr.fer.zemris.ooup.lab4.states.AddShapeState;
import hr.fer.zemris.ooup.lab4.states.EraserState;
import hr.fer.zemris.ooup.lab4.states.IdleState;
import hr.fer.zemris.ooup.lab4.states.SelectShapeState;
import hr.fer.zemris.ooup.lab4.states.State;
import hr.fer.zemris.ooup.lab4.util.IconProvider;

public class JDrawing extends JFrame {

	private DocumentModel model;

	private DrawingCanvas canvas;

	public static State currentState;

	public static final Map<String, GraphicalObject> PROTOTYPES;

	static {
		PROTOTYPES = new HashMap<>();
		PROTOTYPES.put(LineSegment.ID, new LineSegment());
		PROTOTYPES.put(Oval.ID, new Oval());
		PROTOTYPES.put(CompositeShape.ID, new CompositeShape());
	}

	public JDrawing() {
		setTitle("JDRawing");
		setLocation(0, 0);
		setSize(1000, 1000);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());
		currentState = new IdleState();

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// nothing
		}

		initGUI();
	}

	private void initGUI() {
		model = new DocumentModel();
		canvas = new DrawingCanvas(model);
		model.addDocumentModelListener(canvas);
		add(canvas, BorderLayout.CENTER);

		createToolbar();
	}

	private void createToolbar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setFocusable(false);
		toolBar.setRequestFocusEnabled(false);
		add(toolBar, BorderLayout.NORTH);

		JButton open = new JButton();
		open.setToolTipText("Open file");
		open.setIcon(IconProvider.getIcon("open"));
		open.setFocusable(false);
		open.addActionListener(e -> openFile());
		toolBar.add(open);

		JButton save = new JButton();
		save.setToolTipText("Save file");
		save.setIcon(IconProvider.getIcon("save"));
		save.setFocusable(false);
		save.addActionListener(e -> saveFile());
		toolBar.add(save);

		JButton export = new JButton();
		export.setToolTipText("Export to SVG");
		export.setIcon(IconProvider.getIcon("export"));
		export.addActionListener((e) -> exportFile());
		export.setFocusable(false);
		toolBar.add(export);

		toolBar.addSeparator();

		JButton oval = new JButton();
		oval.setIcon(IconProvider.getIcon("oval"));
		oval.setToolTipText("Draw Oval");
		oval.addActionListener(e -> JDrawing.currentState = new AddShapeState(model, new Oval()));
		oval.setFocusable(false);
		toolBar.add(oval);

		JButton line = new JButton();
		line.setIcon(IconProvider.getIcon("line"));
		line.setFocusable(false);
		line.setToolTipText("Draw line segment");
		line.addActionListener(e -> JDrawing.currentState = new AddShapeState(model, new LineSegment()));
		toolBar.add(line);

		toolBar.addSeparator();

		JButton select = new JButton();
		select.setFocusable(false);
		select.setIcon(IconProvider.getIcon("select"));
		select.setToolTipText("Select objects");
		select.addActionListener(e -> JDrawing.currentState = new SelectShapeState(model));
		toolBar.add(select);

		JButton erase = new JButton();
		erase.setFocusable(false);
		erase.setIcon(IconProvider.getIcon("erase"));
		erase.setToolTipText("Erase selected objects");
		erase.addActionListener((e) -> JDrawing.currentState = new EraserState(model));
		toolBar.add(erase);

	}

	private void openFile() {
		JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(this);

		try {
			if (chooser.getSelectedFile() != null && chooser.getSelectedFile().exists()) {

				List<String> lines = Files.readAllLines(chooser.getSelectedFile().toPath());
				Stack<GraphicalObject> stack = new Stack<>();

				for (String line : lines) {
					String elems[] = line.split(" ", 2);
					PROTOTYPES.get(elems[0]).load(stack, elems[1]);
				}

				stack.forEach(o -> model.addGraphicalObject(o));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void exportFile() {
		JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(this);

		try {
			if (chooser.getSelectedFile() != null) {

				if (!chooser.getSelectedFile().exists()) {
					Files.createFile(chooser.getSelectedFile().toPath());
				}

				SVGRendererImpl r = new SVGRendererImpl(chooser.getSelectedFile().getAbsolutePath());
				model.list().forEach(o -> o.render(r));

				r.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void saveFile() {
		JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(this);

		try {
			if (chooser.getSelectedFile() != null) {

				if (!chooser.getSelectedFile().exists()) {
					Files.createFile(chooser.getSelectedFile().toPath());
				}

				List<String> lines = new ArrayList<>();
				model.list().forEach(o -> o.save(lines));
				Files.write(Paths.get(chooser.getSelectedFile().getAbsolutePath()), lines);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JDrawing().setVisible(true));
	}
}
