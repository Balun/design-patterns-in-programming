package hr.fer.zemris.ooup.lab4.drawing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import hr.fer.zemris.ooup.lab4.model.Point;

public class SVGRendererImpl implements Renderer {

	private List<String> lines = new ArrayList<>();
	private String fileName;

	public SVGRendererImpl(String fileName) {
		this.fileName = fileName;
		this.lines = new ArrayList<>();
		lines.add("<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
	}

	public void close() throws IOException {
		lines.add("</svg>");
		Files.write(Paths.get(fileName), lines);
	}

	@Override
	public void drawLine(Point start, Point end) {
		lines.add(String.format("<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:#0000ff;\"/>",
				start.getX(), start.getY(), end.getX(), end.getY()));
	}

	@Override
	public void fillPolygon(Point[] points) {
		StringBuilder sb = new StringBuilder();
		sb.append("<polygon points=\"");

		for (Point point : points) {
			sb.append(point.getX()).append(',').append(point.getY()).append(' ');
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append("\" style=\"stroke:#ff0000; fill:#0000ff;\"/>");
		lines.add(sb.toString());
	}

}
