package hr.fer.zemris.ooup.lab4.drawing;

import static hr.fer.zemris.ooup.lab4.util.GeometryUtil.distanceFromPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hr.fer.zemris.ooup.lab4.listeners.DocumentModelListener;
import hr.fer.zemris.ooup.lab4.listeners.GraphicalObjectListener;
import hr.fer.zemris.ooup.lab4.model.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;

public class DocumentModel {

	public final static double SELECTION_PROXIMITY = 10;

	private List<GraphicalObject> objects;
	private List<GraphicalObject> roObjects;
	private List<DocumentModelListener> listeners;
	private List<GraphicalObject> selectedObjects;
	private List<GraphicalObject> roSelectedObjects;

	private final GraphicalObjectListener goListener = new GraphicalObjectListener() {

		@Override
		public void graphicalObjectSelectionChanged(GraphicalObject go) {
			int index = selectedObjects.indexOf(go);
			if (go.isSelected()) {
				if (index == -1) {
					selectedObjects.add(go);
				} else {
					return;
				}
			} else {
				if (index != -1) {
					selectedObjects.remove(index);
				} else {
					return;
				}
			}
			notifyListeners();
		}

		@Override
		public void graphicalObjectChanged(GraphicalObject go) {
			notifyListeners();
		}
	};

	public DocumentModel() {
		this.objects = new ArrayList<>();
		this.roObjects = Collections.unmodifiableList(objects);
		this.listeners = new ArrayList<>();
		this.selectedObjects = new ArrayList<>();
		this.roSelectedObjects = Collections.unmodifiableList(selectedObjects);
	}

	public void clear() {
		objects.forEach(o -> o.removeGraphicalObjectListener(goListener));
		objects.clear();
		selectedObjects.clear();
		roSelectedObjects = Collections.unmodifiableList(selectedObjects);
		roObjects = Collections.unmodifiableList(objects);
		notifyListeners();
	}

	// Dodavanje objekta u dokument (pazite je li već selektiran; registrirajte
	// model kao promatrača)
	public void addGraphicalObject(GraphicalObject obj) {
		objects.add(obj);
		roObjects = Collections.unmodifiableList(objects);

		if (obj.isSelected()) {
			selectedObjects.add(obj);
			roSelectedObjects = Collections.unmodifiableList(roSelectedObjects);
		}

		obj.addGraphicalObjectListener(goListener);
		notifyListeners();
	}

	// Uklanjanje objekta iz dokumenta (pazite je li već selektiran;
	// odregistrirajte model kao promatrača)
	public void removeGraphicalObject(GraphicalObject obj) {
		obj.setSelected(false);
		obj.removeGraphicalObjectListener(goListener);
		objects.remove(obj);
		roObjects = Collections.unmodifiableList(objects);
//		selectedObjects.remove(obj);
//		roSelectedObjects = Collections.unmodifiableList(selectedObjects);
		notifyListeners();
	}

	public List<GraphicalObject> list() {
		return roObjects;
	}

	public void addDocumentModelListener(DocumentModelListener l) {
		listeners.add(l);
	}

	public void removeDocumentModelListener(DocumentModelListener l) {
		listeners.remove(l);
	}

	public void notifyListeners() {
		listeners.forEach(l -> l.documentChange());
	}

	public List<GraphicalObject> getSelectedObjects() {
		return roSelectedObjects;
	}

	public void increaseZ(GraphicalObject go) {
		int index = objects.indexOf(go);
		if (index != -1 && index < objects.size() - 1) {
			objects.set(index, objects.get(index + 1));
			objects.set(index + 1, go);
		}

		objects = Collections.unmodifiableList(objects);
		notifyListeners();
	}

	public void decreaseZ(GraphicalObject go) {
		int index = objects.indexOf(go);
		if (index != -1 && index > 0) {
			objects.set(index, objects.get(index - 1));
			objects.set(index - 1, go);
		}

		objects = Collections.unmodifiableList(objects);
		notifyListeners();
	}

	public GraphicalObject findSelectedGraphicalObject(Point mousePoint) {
		Map<Double, GraphicalObject> possibleSelections = new HashMap<Double, GraphicalObject>();
		for (GraphicalObject go : objects) {
			double distance = go.selectionDistance(mousePoint);
			if (distance <= SELECTION_PROXIMITY) {
				possibleSelections.put(distance, go);
			}
		}
		if (!possibleSelections.isEmpty()) {
			double min = SELECTION_PROXIMITY + 1;
			for (Entry<Double, GraphicalObject> entry : possibleSelections.entrySet()) {
				if (entry.getKey() < min) {
					min = entry.getKey();
				}
			}
			return possibleSelections.get(min);
		}
		return null;
	}

	public List<GraphicalObject> findSelectedGraphicalObjects(Point mousePoint) {
		List<GraphicalObject> gObjects = new ArrayList<GraphicalObject>();
		for (GraphicalObject go : objects) {
			if (go.selectionDistance(mousePoint) <= SELECTION_PROXIMITY / 2) {
				gObjects.add(go);
			}
		}
		return gObjects;
	}

	public int findSelectedHotPoint(GraphicalObject object, Point mousePoint) {
		for (int i = 0; i < object.getNumberOfHotPoints(); i++) {
			if (distanceFromPoint(object.getHotPoint(i), mousePoint) <= SELECTION_PROXIMITY) {
				return i;
			}
		}
		return -1;
	}

	public void deselectAll() {
		while (selectedObjects.size() > 0) {
			selectedObjects.get(0).setSelected(false);
		}
	}
}
