package hr.fer.zemris.ooup.lab4.drawing;

import java.awt.Color;
import java.awt.Graphics2D;

import hr.fer.zemris.ooup.lab4.model.Point;

public class RendererImpl implements Renderer {

	private Graphics2D g2d;

	public RendererImpl(Graphics2D g2d) {
		this.g2d = g2d;
	}

	@Override
	public void drawLine(Point s, Point e) {
		g2d.setColor(Color.BLUE);
		g2d.drawLine(s.getX(), s.getY(), e.getX(), e.getY());
	}

	@Override
	public void fillPolygon(Point[] points) {
		int[] xpoints = new int[points.length];
		int[] ypoints = new int[points.length];
		for (int i = 0; i < points.length; i++) {
			xpoints[i] = points[i].getX();
			ypoints[i] = points[i].getY();
		}

		g2d.setColor(Color.BLUE);
		g2d.fillPolygon(xpoints, ypoints, points.length);
		g2d.setColor(Color.RED);
		g2d.drawPolygon(xpoints, ypoints, points.length);
	}
}
