package hr.fer.zemris.ooup.lab4.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.ooup.lab4.listeners.GraphicalObjectListener;
import hr.fer.zemris.ooup.lab4.util.GeometryUtil;

import static hr.fer.zemris.ooup.lab4.util.GeometryUtil.*;

public abstract class AbstractGraphicalObject implements GraphicalObject {

	private Point[] hotPoints;
	private boolean[] hotPointSelected;
	private boolean selected;

	List<GraphicalObjectListener> listeners;

	protected AbstractGraphicalObject(Point... points) {
		this.hotPoints = points;
		this.selected = false;
		this.hotPointSelected = new boolean[points.length];
		this.listeners = new LinkedList<>();
	}

	@Override
	public Point[] getHotPoints() {
		return hotPoints;
	}

	@Override
	public Point getHotPoint(int index) throws IndexOutOfBoundsException {
		return hotPoints[index];
	}

	@Override
	public void setHotPoint(int index, Point p) throws IndexOutOfBoundsException {
		hotPoints[index] = p;
		notifyListeners();
	}

	@Override
	public int getNumberOfHotPoints() {
		return 2;
	}

	@Override
	public double getHotPointDistance(int index, Point mousePoint) throws IndexOutOfBoundsException {
		return distanceFromPoint(mousePoint, hotPoints[index]);
	}

	@Override
	public boolean isHotPointSelected(int index) throws IndexOutOfBoundsException {
		return hotPointSelected[index];
	}

	@Override
	public void setHotPointSelected(int index, boolean selected) throws IndexOutOfBoundsException {
		hotPointSelected[index] = selected;
		notifySelectionListeners();
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
		notifySelectionListeners();
	}

	@Override
	public void translate(Point delta) {
		for (int i = 0; i < hotPoints.length; i++) {
			hotPoints[i] = hotPoints[i].translate(delta);
		}
		notifyListeners();
	}

	@Override
	public void addGraphicalObjectListener(GraphicalObjectListener l) {
		listeners.add(l);
	}

	@Override
	public void removeGraphicalObjectListener(GraphicalObjectListener l) {
		listeners.remove(l);
	}

	protected void notifyListeners() {
		listeners.forEach(l -> l.graphicalObjectChanged(AbstractGraphicalObject.this));
	}

	protected void notifySelectionListeners() {
		listeners.forEach(l -> l.graphicalObjectSelectionChanged(AbstractGraphicalObject.this));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(hotPointSelected);
		result = prime * result + Arrays.hashCode(hotPoints);
		result = prime * result + ((listeners == null) ? 0 : listeners.hashCode());
		result = prime * result + (selected ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractGraphicalObject other = (AbstractGraphicalObject) obj;
		if (!Arrays.equals(hotPointSelected, other.hotPointSelected))
			return false;
		if (!Arrays.equals(hotPoints, other.hotPoints))
			return false;
		if (listeners == null) {
			if (other.listeners != null)
				return false;
		} else if (!listeners.equals(other.listeners))
			return false;
		if (selected != other.selected)
			return false;
		return true;
	}

}
