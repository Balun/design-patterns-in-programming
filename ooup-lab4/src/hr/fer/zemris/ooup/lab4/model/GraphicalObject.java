package hr.fer.zemris.ooup.lab4.model;

import java.util.List;
import java.util.Stack;

import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.listeners.GraphicalObjectListener;

public interface GraphicalObject {

	boolean isSelected();

	void setSelected(boolean selected);

	int getNumberOfHotPoints();

	Point[] getHotPoints();

	Point getHotPoint(int index) throws IndexOutOfBoundsException;

	void setHotPoint(int index, Point point) throws IndexOutOfBoundsException;

	boolean isHotPointSelected(int index) throws IndexOutOfBoundsException;

	void setHotPointSelected(int index, boolean selected) throws IndexOutOfBoundsException;

	double getHotPointDistance(int index, Point mousePoint) throws IndexOutOfBoundsException;

	void translate(Point delta);

	Rectangle getBoundingBox();

	double selectionDistance(Point mousePoint);

	void render(Renderer r);

	public void addGraphicalObjectListener(GraphicalObjectListener l);

	public void removeGraphicalObjectListener(GraphicalObjectListener l);

	String getShapeName();

	GraphicalObject duplicate();

	public String getShapeID();

	public void load(Stack<GraphicalObject> stack, String data);

	public void save(List<String> rows);

	public void rotate();
}
