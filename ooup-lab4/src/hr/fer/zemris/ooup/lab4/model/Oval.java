package hr.fer.zemris.ooup.lab4.model;

import hr.fer.zemris.ooup.lab4.drawing.Renderer;

import static hr.fer.zemris.ooup.lab4.util.GeometryUtil.*;

import static hr.fer.zemris.ooup.lab4.util.GeometryUtil.*;

import java.util.List;
import java.util.Stack;

public class Oval extends AbstractGraphicalObject implements Cloneable {

	public static final String ID = "@OVAL";

	private final static int NUMBER_OF_POINTS = 50;

	public Oval(Point right, Point down) {
		super(right, down);
	}

	public Oval() {
		this(new Point(0, 10), new Point(10, 0));
	}

	@Override
	public Rectangle getBoundingBox() {
		Point lowerHotPoint = getHotPoint(0);
		Point rightHotPoint = getHotPoint(1);

		int x = lowerHotPoint.getX() - (rightHotPoint.getX() - lowerHotPoint.getX());
		int y = rightHotPoint.getY() - (lowerHotPoint.getY() - rightHotPoint.getY());

		int width = 2 * (rightHotPoint.getX() - lowerHotPoint.getX());
		int height = 2 * (lowerHotPoint.getY() - rightHotPoint.getY());

		return new Rectangle(x, y, width, height);
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		// check if it's clicked inside oval
		Point lowerHotPoint = getHotPoint(0);
		Point rightHotPoint = getHotPoint(1);
		int a = rightHotPoint.getX() - lowerHotPoint.getX();
		int b = lowerHotPoint.getY() - rightHotPoint.getY();
		int p = lowerHotPoint.getX();
		int q = rightHotPoint.getY();
		double elipseEquation = Math.pow(mousePoint.getX() - p, 2) / Math.pow(a, 2)
				+ Math.pow(mousePoint.getY() - q, 2) / Math.pow(b, 2);
		if (elipseEquation <= 1) {
			return 0;// click is inside oval
		}

		Point[] points = getPoints(20);
		double min = distanceFromPoint(points[0], mousePoint);
		for (int i = 1; i < points.length; i++) {
			double distance = distanceFromPoint(points[i], mousePoint);
			if (distance < min) {
				min = distance;
			}
		}
		return min;
	}

	private Point[] getPoints(int numOfPoints) {
		Point lowerHotPoint = getHotPoint(0);
		Point rightHotPoint = getHotPoint(1);
		Point center = new Point(lowerHotPoint.getX(), rightHotPoint.getY());
		int a = rightHotPoint.getX() - lowerHotPoint.getX();
		int b = lowerHotPoint.getY() - rightHotPoint.getY();

		Point[] points = new Point[numOfPoints];
		for (int i = 0; i < numOfPoints; i++) {
			double t = (2 * Math.PI / numOfPoints) * i;
			int x = (int) (a * Math.cos(t)) + center.getX();
			int y = (int) (b * Math.sin(t)) + center.getY();
			points[i] = new Point(x, y);
		}
		return points;
	}

	@Override
	public void render(Renderer r) {
		r.fillPolygon(getPoints(NUMBER_OF_POINTS));
	}

	@Override
	public String getShapeName() {
		return "oval";
	}

	@Override
	public GraphicalObject duplicate() {
		try {
			return (GraphicalObject) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getShapeID() {
		return ID;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Oval(getHotPoint(0), getHotPoint(1));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ID == null) ? 0 : ID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Oval other = (Oval) obj;
		if (ID == null) {
			if (other.ID != null)
				return false;
		} else if (!ID.equals(other.ID))
			return false;
		return true;
	}

	@Override
	public void save(List<String> rows) {
		Point lowerHotPoint = getHotPoint(0);
		Point rightHotPoint = getHotPoint(1);
		rows.add(String.format("%s %d %d %d %d", getShapeID(), rightHotPoint.getX(), rightHotPoint.getY(),
				lowerHotPoint.getX(), lowerHotPoint.getY()));
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		String[] coordinates = data.trim().split(" ");
		Point first = new Point(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]));
		Point second = new Point(Integer.parseInt(coordinates[2]), Integer.parseInt(coordinates[3]));
		stack.push(new Oval(first, second));
	}

	@Override
	public void rotate() {
		// TODO Auto-generated method stub
		
	}

}
