package hr.fer.zemris.ooup.lab4.model;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.listeners.GraphicalObjectListener;

public class CompositeShape implements GraphicalObject {

	private List<GraphicalObject> objects;
	private boolean selected;
	private List<GraphicalObjectListener> listeners;

	public static final String ID = "@COMP";

	public CompositeShape() {
		this(false);
	}

	public CompositeShape(boolean selected, GraphicalObject... objects) {
		this.selected = selected;
		this.listeners = new LinkedList<>();
		this.objects = Arrays.asList(objects);
	}

	@Override
	public boolean isSelected() {
		return selected;
	}

	@Override
	public void setSelected(boolean selected) {
		this.selected = selected;
		notifySelectionListeners();
	}

	@Override
	public int getNumberOfHotPoints() {
		return 0;
	}

	@Override
	public Point[] getHotPoints() {
		return null;
	}

	@Override
	public Point getHotPoint(int index) throws IndexOutOfBoundsException {
		return null;
	}

	@Override
	public void setHotPoint(int index, Point point) throws IndexOutOfBoundsException {
	}

	@Override
	public boolean isHotPointSelected(int index) throws IndexOutOfBoundsException {
		return false;
	}

	@Override
	public void setHotPointSelected(int index, boolean selected) throws IndexOutOfBoundsException {
	}

	@Override
	public double getHotPointDistance(int index, Point mousePoint) throws IndexOutOfBoundsException {
		return Double.MAX_VALUE;
	}

	@Override
	public void translate(Point delta) {
		for (GraphicalObject object : objects) {
			object.translate(delta);
		}
		notifyObservers();
	}

	@Override
	public Rectangle getBoundingBox() {
		Rectangle rect = objects.get(0).getBoundingBox();
		int minX = (int) rect.getMinX();
		int maxX = (int) rect.getMaxX();
		int minY = (int) rect.getMinY();
		int maxY = (int) rect.getMaxY();

		for (int i = 1; i < objects.size(); i++) {
			rect = objects.get(0).getBoundingBox();
			minX = rect.getMinX() < minX ? (int) rect.getMinX() : minX;
			maxX = rect.getMaxX() > maxX ? (int) rect.getMaxX() : maxX;
			minY = rect.getMinY() < minY ? (int) rect.getMinY() : minY;
			maxY = rect.getMaxY() > maxY ? (int) rect.getMaxY() : maxY;
		}

		return new Rectangle(minX, minY, maxX - minX, maxY - minY);
	}

	public List<GraphicalObject> getObjects() {
		return objects;
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		if (objects.size() == 0) {
			return Double.MAX_VALUE;
		}

		double[] distances = new double[objects.size()];
		for (int i = 0; i < objects.size(); i++) {
			distances[i] = objects.get(0).selectionDistance(mousePoint);
		}

		double min = distances[0];
		for (int i = 1; i < distances.length; i++) {
			if (distances[i] < min) {
				min = distances[i];
			}
		}
		return min;
	}

	@Override
	public void render(Renderer r) {
		objects.forEach(o -> o.render(r));
	}

	@Override
	public void addGraphicalObjectListener(GraphicalObjectListener l) {
		listeners.add(l);
	}

	@Override
	public void removeGraphicalObjectListener(GraphicalObjectListener l) {
		listeners.remove(l);
	}

	@Override
	public String getShapeName() {
		return "composite";
	}

	@Override
	public GraphicalObject duplicate() {
		List<GraphicalObject> duplicates = new ArrayList<>();
		objects.forEach(o -> duplicates.add(o.duplicate()));
		return new CompositeShape(false, duplicates.toArray(new GraphicalObject[duplicates.size()]));

	}

	@Override
	public String getShapeID() {
		return ID;
	}

	private void notifyObservers() {
		for (GraphicalObjectListener graphicalObjectListener : listeners) {
			graphicalObjectListener.graphicalObjectChanged(this);
		}
	}

	public void notifySelectionListeners() {
		for (GraphicalObjectListener listener : listeners) {
			listener.graphicalObjectSelectionChanged(this);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((listeners == null) ? 0 : listeners.hashCode());
		result = prime * result + ((objects == null) ? 0 : objects.hashCode());
		result = prime * result + (selected ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompositeShape other = (CompositeShape) obj;
		if (listeners == null) {
			if (other.listeners != null)
				return false;
		} else if (!listeners.equals(other.listeners))
			return false;
		if (objects == null) {
			if (other.objects != null)
				return false;
		} else if (!objects.equals(other.objects))
			return false;
		if (selected != other.selected)
			return false;
		return true;
	}

	@Override
	public void save(List<String> rows) {
		objects.forEach(o -> o.save(rows));
		rows.add(String.format("%s %d", getShapeID(), objects.size()));
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		int numOfObjects = Integer.parseInt(data.trim());
		GraphicalObject[] gObjects = new GraphicalObject[numOfObjects];

		for (int i = 0; i < numOfObjects; i++) {
			gObjects[i] = stack.pop();
		}

		stack.push(new CompositeShape(false, gObjects));
	}

	@Override
	public void rotate() {
		// TODO Auto-generated method stub
		
	}

}
