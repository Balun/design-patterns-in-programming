package hr.fer.zemris.ooup.lab4.model;

import static hr.fer.zemris.ooup.lab4.util.GeometryUtil.*;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.ooup.lab4.drawing.Renderer;

public class LineSegment extends AbstractGraphicalObject implements Cloneable {

	public static final String ID = "@LINE";

	public LineSegment(Point start, Point end) {
		super(start, end);
	}

	public LineSegment() {
		this(new Point(0, 0), new Point(100, 0));
	}

	@Override
	public Rectangle getBoundingBox() {
		Point point1 = getHotPoint(0);
		Point point2 = getHotPoint(1);

		int x = point1.getX() < point2.getX() ? point1.getX() : point2.getX();
		int y = point1.getY() < point2.getY() ? point1.getY() : point2.getY();

		return new Rectangle(x, y, Math.abs(point1.getX() - point2.getX()), Math.abs(point1.getY() - point2.getY()));
	}

	@Override
	public double selectionDistance(Point mousePoint) {
		return distanceFromLineSegment(getHotPoint(0), getHotPoint(1), mousePoint);
	}

	@Override
	public void render(Renderer r) {
		r.drawLine(getHotPoint(0), getHotPoint(1));
	}

	@Override
	public String getShapeName() {
		return "line";
	}

	@Override
	public GraphicalObject duplicate() {
		try {
			return (GraphicalObject) this.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public String getShapeID() {
		return ID;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new LineSegment(getHotPoint(0), getHotPoint(1));
	}

	@Override
	public void save(List<String> rows) {
		Point start = getHotPoint(0);
		Point end = getHotPoint(1);
		rows.add(String.format("%s %d %d %d %d", getShapeID(), start.getX(), start.getY(), end.getX(), end.getY()));
	}

	@Override
	public void load(Stack<GraphicalObject> stack, String data) {
		String[] coordinates = data.trim().split(" ");
		Point first = new Point(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1]));
		Point second = new Point(Integer.parseInt(coordinates[2]), Integer.parseInt(coordinates[3]));// end
		stack.push(new LineSegment(first, second));
	}

	@Override
	public void rotate() {
		AffineTransform at = AffineTransform.getRotateInstance(Math.toRadians(90), getHotPoint(0).getX(),
				getHotPoint(0).getY());

		Shape s = at.createTransformedShape(new Line2D.Double(getHotPoint(0).getX(), getHotPoint(0).getY(),
				getHotPoint(1).getX(), getHotPoint(1).getY()));

		setHotPoint(1, new Point(s.getBounds().x + s.getBounds().width, s.getBounds().y + s.getBounds().height));
		notifyListeners();
	}

}
