package hr.fer.zemris.ooup.lab4.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;

import hr.fer.zemris.ooup.lab4.JDrawing;
import hr.fer.zemris.ooup.lab4.drawing.DocumentModel;
import hr.fer.zemris.ooup.lab4.drawing.Renderer;
import hr.fer.zemris.ooup.lab4.drawing.RendererImpl;
import hr.fer.zemris.ooup.lab4.listeners.DocumentModelListener;
import hr.fer.zemris.ooup.lab4.model.GraphicalObject;
import hr.fer.zemris.ooup.lab4.model.Point;
import hr.fer.zemris.ooup.lab4.states.IdleState;

public class DrawingCanvas extends JComponent implements DocumentModelListener {

	private DocumentModel model;

	public DrawingCanvas(DocumentModel model) {
		super();
		this.model = model;
		setOpaque(true);

		setKeyListener();
		setMouseListener();
		setMouseMotionListener();
		setFocusable(true);
	}

	private void setMouseMotionListener() {
		addMouseMotionListener(new MouseAdapter() {

			@Override
			public void mouseDragged(MouseEvent e) {
				JDrawing.currentState.mouseDragged(new Point(e.getX(), e.getY()));
			}
		});
	}

	private void setMouseListener() {
		addMouseListener(new MouseAdapter() {

			@Override
			public void mouseReleased(MouseEvent e) {
				JDrawing.currentState.mouseUp(new Point(e.getX(), e.getY()), e.isShiftDown(), e.isControlDown());
			}

			@Override
			public void mousePressed(MouseEvent e) {
				JDrawing.currentState.mouseDown(new Point(e.getX(), e.getY()), e.isShiftDown(), e.isControlDown());
			}
		});
	}

	private void setKeyListener() {
		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					JDrawing.currentState.onLeaving();
					JDrawing.currentState = new IdleState();
				} else {
					JDrawing.currentState.keyPressed(e.getKeyCode());
				}
			}
		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);
		g2.fillRect(0, 0, getWidth(), getHeight());

		Renderer rend = new RendererImpl(g2);
		for (GraphicalObject obj : model.list()) {
			obj.render(rend);
			JDrawing.currentState.afterDraw(rend, obj);
		}

		JDrawing.currentState.afterDraw(rend);
		requestFocusInWindow();
	}

	@Override
	public void documentChange() {
		repaint();
	}

}
