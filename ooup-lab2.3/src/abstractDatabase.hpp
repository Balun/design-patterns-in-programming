/*
 * abstractDatabase.hpp
 *
 *  Created on: Apr 13, 2017
 *      Author: lumba
 */

#ifndef ABSTRACTDATABASE_HPP_
#define ABSTRACTDATABASE_HPP_


class AbstractDatabase{

public:
	~AbstractDatabase() = default;
	virtual const char* getData() = 0;
};


#endif /* ABSTRACTDATABASE_HPP_ */
