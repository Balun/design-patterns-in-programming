//============================================================================
// Name        : 3.cpp
// Author      : Matej Balun
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "abstractDatabase.hpp"
#include "mockDatabase.hpp"
#include "client2.hpp"
using namespace std;

void assertGetDataWasCalled(AbstractDatabase& db){
	cout << "assertion:" << endl;
	cout << db.getData() << endl;
}

int main() {

	// construct database
	MockDatabase* pdb = new MockDatabase();

	// construct test object
	// ( dependency injection )
	Client2 client(*pdb);

	// test behaviour #1
	client.transaction();

	assertGetDataWasCalled(*pdb);
	// test behaviour #2
	// ...
}
