/*
 * mockDatabase.hpp
 *
 *  Created on: Apr 13, 2017
 *      Author: lumba
 */

#include "abstractDatabase.hpp"
#ifndef MOCKDATABASE_HPP_
#define MOCKDATABASE_HPP_


class MockDatabase : public AbstractDatabase{

public:
	MockDatabase() :  AbstractDatabase(){
	}

	virtual const char* getData();
};


#endif /* MOCKDATABASE_HPP_ */
