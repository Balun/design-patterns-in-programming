/*
 * client2.hpp
 *
 *  Created on: Apr 13, 2017
 *      Author: lumba
 */

#include "abstractDatabase.hpp"
#include <iostream>

using namespace std;

#ifndef CLIENT2_HPP_
#define CLIENT2_HPP_

class Client2 {
	AbstractDatabase& myDatabase;

public:
	Client2(AbstractDatabase& db) :
			myDatabase(db) {
	}

	void transaction() {
		cout << myDatabase.getData() << endl;
		// ...
	}

	// ...
};

#endif /* CLIENT2_HPP_ */
