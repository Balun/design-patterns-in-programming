package hr.fer.zemris.ooup.lab2.zad5;

public interface INumberAction {
	
	void apply(NumberSequence sequence);
	
	String getName();

}
