package hr.fer.zemris.ooup.lab2.zad5;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class NumberSequence {

	private List<Integer> list;

	private List<INumberAction> actions;

	private InputStream input;

	public NumberSequence(InputStream input, INumberAction... actions) {
		this.list = new ArrayList<>();
		this.actions = new ArrayList<>(Arrays.asList(actions));
		this.input = input;
	}

	public void start() throws NumberFormatException, IOException {

		Scanner sc = new Scanner(input);

		while (true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("Unosim podatak...");

			if (sc.hasNextLine()) {

				String line = sc.nextLine();
				if (line.equalsIgnoreCase("exit")) {
					System.out.println("Ulaz zatvoren.");
					break;
				}

				int n = Integer.parseInt(line);

				if (n < 0) {
					System.err.println("Broj mora biti ne negativan!");
					continue;
				}

				list.add(n);
				actions.forEach(a -> a.apply(NumberSequence.this));
			}
		}

		sc.close();
	}

	public void changeInputSource(InputStream input) {
		this.input = input;
	}

	public void addAction(INumberAction action) {
		this.actions.add(action);
	}

	public void removeAction(INumberAction action) {
		this.list.remove(action);
	}

	public List<Integer> getList() {
		return this.list;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		for (int n : this.list) {
			builder.append(n + ", ");
		}

		builder.delete(builder.length() - 2, builder.length());
		builder.append("\n");
		return builder.toString();
	}

}
