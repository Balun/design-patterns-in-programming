package hr.fer.zemris.ooup.lab2.zad4;

import java.util.ArrayList;
import java.util.List;

public class GenerateSequential implements INumberGenerator {

	private int upper;
	private int lower;
	private int step;

	public GenerateSequential(int lower, int upper, int step) {
		this.lower = lower;
		this.upper = upper;
		this.step = step;
	}

	@Override
	public List<Integer> generateNumbers() {
		List<Integer> list = new ArrayList<>();
		for (int i = lower; i <= upper; i += step) {
			list.add(i);
		}

		return list;
	}

}
