package hr.fer.zemris.ooup.lab2.zad4;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateRandom implements INumberGenerator {

	private int n;

	public GenerateRandom(int n) {
		this.n = n;
	}

	@Override
	public List<Integer> generateNumbers() {
		Random rand = new Random();
		List<Integer> list = new ArrayList<>();

		for (int i = 0; i < n; i++) {
			list.add((int) Math.round(rand.nextGaussian()));
		}

		return list;
	}

}
