package hr.fer.zemris.ooup.lab2.zad4;

import java.util.ArrayList;
import java.util.List;

public class GenerateFibonnaci implements INumberGenerator {

	private int n;

	public GenerateFibonnaci(int n) {
		this.n = n;
	}

	@Override
	public List<Integer> generateNumbers() {
		List<Integer> list = new ArrayList<>();
		int first = 0;
		int second = 1;

		if (n == 1) {
			list.add(first);
		} else if (n == 2) {
			list.add(first);
			list.add(second);

		} else {
			list.add(first);
			list.add(second);
			for (int i = 2; i < n; i++) {
				list.add(list.get(i - 1) + list.get(i - 2));
			}
		}

		return list;
	}

}
