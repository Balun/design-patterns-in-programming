package hr.fer.zemris.ooup.lab2.zad5;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.Scanner;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class demo {

	public static class PrintToFileAction extends AbstractNumberAction {

		private Path file;

		public PrintToFileAction(String name, String filePath) {
			super(name);
			this.file = Paths.get(filePath);
		}

		@Override
		public void apply(NumberSequence sequence) {
			try {
				Files.write(file, (new Date().toString() + "\n").getBytes(), StandardOpenOption.APPEND);
				Files.write(file, sequence.getList().toString().getBytes(), StandardOpenOption.APPEND);
			} catch (IOException e) {
				System.err.println("Error in writing to file");
			}
		}
	}

	public static class WriteSumAction extends AbstractNumberAction {

		public WriteSumAction(String name) {
			super(name);
		}

		@Override
		public void apply(NumberSequence sequence) {
			int sum = sequence.getList().stream().mapToInt(Integer::intValue).sum();
			System.out.println("Suma svih elemenata je: " + sum);
		}

	}

	public static class WriteAverageAction extends AbstractNumberAction {

		public WriteAverageAction(String name) {
			super(name);
		}

		@Override
		public void apply(NumberSequence sequence) {
			double av = sequence.getList().stream().mapToInt(Integer::intValue).average().getAsDouble();
			System.out.printf("Prosjek svih elemenata je: %.2f\n", av);
		}

	}

	public static class WriteMedianAction extends AbstractNumberAction {

		public WriteMedianAction(String name) {
			super(name);
		}

		@Override
		public void apply(NumberSequence sequence) {
			double[] array = sequence.getList().stream().mapToDouble(Integer::intValue).toArray();
			int median = (int) new DescriptiveStatistics(array).getPercentile(50);
			System.out.println("Medijan svih elemenata je: " + median);
		}

	}

	public static void main(String[] args) throws NumberFormatException, IOException {

		while (true) {
			try (Scanner sc = new Scanner(System.in)) {
				System.out.println("Odaberite način unosa podataka; A -> preko tipkovnice, B -> iz datoteke");
				String option = "";

				if (sc.hasNextLine()) {
					option = sc.nextLine();
					sc.close();

					switch (option.toUpperCase()) {

					case "A":
						NumberSequence seq1 = new NumberSequence(System.in, new WriteAverageAction("prosjek"),
								new WriteMedianAction("median"), new WriteSumAction("suma"),
								new PrintToFileAction("printaj", "izlaz.txt"));
						seq1.start();
						return;

					case "B":
						NumberSequence seq2 = new NumberSequence(new FileInputStream("ulaz.txt"),
								new WriteAverageAction("prosjek"), new WriteMedianAction("median"),
								new WriteSumAction("suma"), new PrintToFileAction("printaj", "izlaz.txt"));
						seq2.start();
						return;

					default:
						continue;
					}
				}
			}
		}
	}

}
