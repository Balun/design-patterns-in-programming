package hr.fer.zemris.ooup.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) throws IOException {

		Scanner sc = new Scanner(System.in);
		String line = null;

		while (sc.hasNextLine()) {
			System.out.println(sc.nextLine());
		}

		sc.close();
	}
}
