/*
 * drugi.cpp
 *
 *  Created on: Apr 12, 2017
 *      Author: lumba
 */

#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

typedef struct {
	int x;
	int y;
} Point;

class Shape {
public:
	virtual void drawShape() = 0;
};

class Circle: public Shape {
public:
	Point center;
	int radius;

	Circle(int x, int y, int radius_) {
		center.x = x;
		center.y = y;
		radius = radius_;
	}

	virtual void drawShape() {
		printf("Circle with center %d,%d and radius %d\n", center.x, center.y,
				radius);
	}
};

class Square: public Shape {
public:
	Point center;
	int side;

	Square(int x, int y, int side_) {
		center.x = x;
		center.y = y;
		side = side_;
	}

	virtual void drawShape() {
		printf("Square with upper left corner %d,%d and side %d\n", center.x,
				center.y, side);
	}
};

class Rhomb: public Square {
public:
	Rhomb(int x, int y, int side_) :
			Square(x, y, side_) {
	}

	virtual void drawShape() {
		printf("Rhomb with center %d, %d and side %d\n", center.x, center.y,
				side);
	}

};

void drawShapes(Shape** shapes, int n) {
	for (int i = 0; i < n; i++) {
		Shape* s = shapes[i];
		s->drawShape();
	}
}

int main() {
	Shape* shapes[5];
	shapes[0] = new Circle(0, 0, 5);
	shapes[1] = new Square(0, 0, 5);
	shapes[2] = new Square(2, 2, 3);
	shapes[3] = new Circle(1, 1, 2);
	shapes[4] = new Rhomb(3, 3, 4);

	drawShapes(shapes, 5);
}
