//============================================================================
// Name        : 2.cpp
// Author      : Matej Balun
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

using namespace std;

struct Point {
	int x;
	int y;
};
struct Shape {
	enum EType {
		circle, square, rhomb
	};
	EType type_;
};
struct Circle {
	Shape::EType type_;
	double radius_;
	Point center_;
};
struct Square {
	Shape::EType type_;
	double side_;
	Point center_;
};

struct Rhomb {
	Shape::EType type_;
	double side_;
	Point center_;
};

void drawSquare(struct Square*) {
	std::cerr << "in drawSquare\n";
}
void drawCircle(struct Circle*) {
	std::cerr << "in drawCircle\n";
}

void drawRhomb(struct Rhomb*) {
	cerr << "in drawRhomb\n";
}

void drawShapes(Shape** shapes, int n) {
	for (int i = 0; i < n; ++i) {
		struct Shape* s = shapes[i];
		switch (s->type_) {
		case Shape::square:
			drawSquare((struct Square*) s);
			break;
		case Shape::circle:
			drawCircle((struct Circle*) s);
			break;
		case Shape::rhomb:
			drawRhomb((struct Rhomb*) s);
			break;
		default:
			assert(0);
			exit(0);
		}
	}
}

void moveShapes(Shape** shapes, int n, int translate) {
	for (int i = 0; i < n; ++i) {
		struct Shape* s = shapes[i];
		switch (s->type_) {
		case Shape::square: {
			struct Square* square = (struct Square*) s;
			square->center_.x = square->center_.x + translate;
			printf("new square with center %d, %d\n", square->center_.x,
					square->center_.y);
			break;
		}
		case Shape::circle: {
			struct Circle* circle = (struct Circle*) s;
			circle->center_.x = circle->center_.x + translate;
			printf("new circle with center %d, %d\n", circle->center_.x,
					circle->center_.y);
			break;
		}
		case Shape::rhomb: {
			struct Rhomb* rhomb = (struct Rhomb*) s;
			rhomb->center_.x = rhomb->center_.x + translate;
			printf("new rhomb with center %d, %d\n", rhomb->center_.x,
					rhomb->center_.y);
			break;
		}
		default:

			assert(0);
			exit(0);
		}
	}
}

//int main() {
//	Shape* shapes[5];
//	shapes[0] = (Shape*) new Circle;
//	shapes[0]->type_ = Shape::circle;
//	shapes[1] = (Shape*) new Square;
//	shapes[1]->type_ = Shape::square;
//	shapes[2] = (Shape*) new Square;
//	shapes[2]->type_ = Shape::square;
//	shapes[3] = (Shape*) new Circle;
//	shapes[3]->type_ = Shape::circle;
//	shapes[4] = (Shape*) new Rhomb;
//	shapes[4]->type_ = Shape::rhomb;
//
//	drawShapes(shapes, 5);
//	moveShapes(shapes, 5, 3);
//}
