#include <stdio.h>
#include <stdlib.h>

#define VALUE_AT 0
#define NEG_VALUE_AT 1
#define TABULATE 2
#define FOR_INTS 3

typedef void (*funptr)();

/* ==============================================
 RAZRED UnaryFunction
 ============================================== */

typedef struct {
	funptr*vtable;
	int lower_bound;
	int upper_bound;
} UnaryFunction;

typedef void (*funptrVOID)(UnaryFunction*);
typedef int (*funptrBOOL)(UnaryFunction*, UnaryFunction*, double);
typedef double (*funptrDOUB)(UnaryFunction*, double);

double unaryValueAt(double x) {
	return 0;
}

double unaryNegativeValueAt(UnaryFunction *obj, double x) {
	return -((funptrDOUB) obj->vtable[VALUE_AT])(obj, x);
}

int sameFunctionsForInts(UnaryFunction *f1, UnaryFunction *f2, double tolerance) {
	if (f1->lower_bound != f2->lower_bound) {
		return 0;
	}
	if (f1->upper_bound != f2->upper_bound) {
		return 0;
	}
	for (int x = f1->lower_bound; x <= f1->upper_bound; x++) {
		double delta = ((funptrDOUB) f1->vtable[VALUE_AT])(f1, x)
				- ((funptrDOUB) f2->vtable[VALUE_AT])(f2, x);
		if (delta < 0) {
			delta = -delta;
		}
		if (delta > tolerance) {
			return 0;
		}
	}

	return 1;
}

void tabulate(UnaryFunction*obj) {
	for (int x = obj->lower_bound; x <= obj->upper_bound; x++) {
		printf("f(%d)=%lf\n", x, ((funptrDOUB) obj->vtable[VALUE_AT])(obj, x));
	}
}

funptr unaryVTable[2] = { NULL, (funptr) unaryNegativeValueAt};

void unaryFunctionInit(UnaryFunction* obj, int lb, int ub) {
	obj->vtable = unaryVTable;
	obj->lower_bound = lb;
	obj->upper_bound = ub;
}

/* ==============================================
 RAZRED Square
 ============================================== */

typedef struct {
	funptr *vtable;
	int lower_bound;
	int upper_bound;
} Square;

double squareValueAt(Square *obj, double x) {
	return x * x;
}

funptr squareVTable[2] = { (funptr) squareValueAt,
		(funptr) unaryNegativeValueAt};

void squareInit(Square *obj, int lb, int ub) {
	unaryFunctionInit((UnaryFunction*) obj, lb, ub);
	obj->vtable = squareVTable;
}

/* ==============================================
 RAZRED Linear
 ============================================== */

typedef struct {
	funptr *vtable;
	int lower_bound;
	int upper_bound;
	double a;
	double b;
} Linear;

double LinearValueAt(Linear *obj, double x) {
	return obj->a * x + obj->b;
}

funptr linearVTable[2] = { (funptr) LinearValueAt,
		(funptr) unaryNegativeValueAt };

UnaryFunction* linearInit(Linear* obj, int lb, int ub, double aCoef,
		double bCoef) {
	unaryFunctionInit((UnaryFunction*) obj, lb, ub);
	obj->vtable = linearVTable;
	obj->a = aCoef;
	obj->b = bCoef;
	return NULL;
}

/* ==============================================
 METODA MAIN
 ============================================== */

int main() {
	UnaryFunction *f1 = (UnaryFunction*) malloc(sizeof(Square));
	squareInit((Square*) f1, -2, 2);
	tabulate(f1);

	UnaryFunction *f2 = (UnaryFunction*) malloc(sizeof(Linear));
	linearInit((Linear*) f2, -2, 2, 5, -2);
	tabulate(f2);

	printf("f1==f2: %s\n", sameFunctionsForInts(f1, f2, 1E-6) ? "DA" : "NE");
	printf("neg_val f2(1) = %lf\n",
			((funptrDOUB) f2->vtable[NEG_VALUE_AT])(f2, 1.0));

	free(f1);
	free(f2);
	return 0;
}
