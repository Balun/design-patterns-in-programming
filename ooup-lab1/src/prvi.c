#include <stdio.h>
#include <stdlib.h>

typedef void (*FUNPTR)();

//Razred Animal
typedef struct {
	FUNPTR *vtable;
	char const* name;
} Animal;

typedef char const* (*funptrVF)(Animal*);

void animalPrintGreeting(Animal *obj) {
	funptrVF pfun = (funptrVF) obj->vtable[0];
	printf("%s pozdravlja: %s\n", obj->name, pfun(obj));
}

void animalPrintMenu(Animal *obj) {
	funptrVF pfun = (funptrVF) obj->vtable[1];
	printf("%s voli %s\n", obj->name, pfun(obj));
}

FUNPTR AnimalVTable[2] = { (FUNPTR) animalPrintGreeting,
		(FUNPTR) animalPrintMenu };

void constructAnimal(Animal* obj, char const* name){
	obj->vtable = AnimalVTable;
	obj->name = name;
}

//razred Dog

char const* DogGreet(Animal *obj) {
	return "vau!";
}

char const* DogMenu(Animal *obj) {
	return "kuhanu govedinu";
}

FUNPTR DogVTable[2] = { (FUNPTR) DogGreet, (FUNPTR) DogMenu };

void constructDog(Animal* dog, char const* name) {
	constructAnimal(dog, name);
	dog->vtable = DogVTable;
}

Animal* createDog(char const* name) {
	Animal *dog = (Animal*) malloc(sizeof(Animal));
	constructDog(dog, name);
	return dog;
}

//razred Cat

char const* CatGreet(Animal *obj) {
	return "mijau!";
}

char const* CatMenu(Animal *obj) {
	return "konzerviranu tunjevinu";
}

FUNPTR CatVTable[2] = { (FUNPTR) CatGreet, (FUNPTR) CatMenu };

void constructCat(Animal* cat, char const* name) {
	constructAnimal(cat, name);
	cat->vtable = CatVTable;
}

Animal* createCat(char const* name) {
	Animal *cat = (Animal*) malloc(sizeof(Animal));
	constructCat(cat, name);
	return cat;
}

void testAnimals(void) {

	printf("Ipis sa alokacijom na heap-u:\n\n");

	Animal* p1 = createDog("Hamlet");
	Animal* p2 = createCat("Ofelija");
	Animal* p3 = createDog("Polonije");

	animalPrintGreeting(p1);
	animalPrintGreeting(p2);
	animalPrintGreeting(p3);

	animalPrintMenu(p1);
	animalPrintMenu(p2);
	animalPrintMenu(p3);

	free(p1);
	free(p2);
	free(p3);
}

void testAnimanlsStack() {

	printf("\nispis pomoću stoga:\n\n");

	Animal p1, p2, p3;

	constructDog(&p1, "Hamlet");
	constructCat(&p2, "Ofelija");
	constructDog(&p3, "Polonije");

	animalPrintGreeting(&p1);
	animalPrintGreeting(&p2);
	animalPrintGreeting(&p3);

	animalPrintMenu(&p1);
	animalPrintMenu(&p2);
	animalPrintMenu(&p3);
}

void createNDogs(int n) {
	printf("Kreiranje %d pasa:\n\n", n);
	Animal *dogs = (Animal *) malloc(n * sizeof(Animal));

	for (int i = 0; i < n; i++) {
		char str[4];
		sprintf(str, "pas%d", i);
		constructDog(&dogs[i], str);
		animalPrintGreeting(&dogs[i]);
	}
}

int main(int argc, char **argv) {
	testAnimals();
	testAnimanlsStack();
	createNDogs(6);
	printf("\nveličina razreda Animal je: %ld\n", sizeof(Animal));
	return 0;
}

