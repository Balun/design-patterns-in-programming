package hr.fer.zemris.ooup.lab2.zad5;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NumberSequence {

	private List<Integer> list;

	private InputStream input;

	public NumberSequence(InputStream input) {
		this.list = new ArrayList<>();
		this.input = input;
	}

	public void start() throws NumberFormatException, IOException {

		try (Scanner sc = new Scanner(input)) {

			while (sc.hasNextInt()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					System.err.println("Sleep interupted");
					System.exit(-1);
				}

				int n = sc.nextInt();
				if (n < 0) {
					throw new NumberFormatException("The source should generate only non-negative numbers!");
				} else {
					list.add(n);
				}
			}
		}
	}

	public void changeInputSource(InputStream input) {
		this.input = input;
	}

	public void applyAction(INumberAction action) {
		action.apply(this);
		System.out.println(action.getName());
	}

	public List<Integer> getList() {
		return this.list;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for (int n : this.list) {
			builder.append(n + ", ");
		}

		builder.delete(builder.length() - 2, builder.length());
		builder.append("\n");
		return builder.toString();
	}

}
