package hr.fer.zemris.ooup.lab2.zad5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.stream.Collectors;

public class demo {

	public static class PrintToFileAction extends AbstractNumberAction {

		private Path file;

		public PrintToFile(String name, String filePath) {
			super(name);
			this.file = Paths.get(filePath);
		}

		@Override
		public void apply(NumberSequence sequence) {
			try {
				Files.write(file, sequence.toString().getBytes());
				Files.write(file, new Date().toString().getBytes());
			} catch (IOException e) {
				System.err.println("Error in writing to file");
			}
		}
	}

	public static class WriteSumAction extends AbstractNumberAction {

		public WriteSumAction(String name) {
			super(name);
		}

		@Override
		public void apply(NumberSequence sequence) {
			int sum = sequence.getList().stream().mapToInt(Integer::intValue).sum();
			System.out.println("Suma svih elemenata je: " + sum);
		}

	}

	public static class WriteAverageAction extends AbstractNumberAction {

		public WriteAverageAction(String name) {
			super(name);
		}

		@Override
		public void apply(NumberSequence sequence) {
			double av = sequence.getList().stream().mapToInt(Integer::intValue).average().getAsDouble();
			System.out.printf("Prosjek svih elemenata je: %.2f", av);
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
