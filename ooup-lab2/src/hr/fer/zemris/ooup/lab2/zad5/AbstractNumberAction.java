package hr.fer.zemris.ooup.lab2.zad5;

public abstract class AbstractNumberAction implements INumberAction {

	private String name;

	public AbstractNumberAction(String name) {
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
}
