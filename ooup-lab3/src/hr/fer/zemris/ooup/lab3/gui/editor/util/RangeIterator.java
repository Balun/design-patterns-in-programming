package hr.fer.zemris.ooup.lab3.gui.editor.util;

import java.util.Iterator;
import java.util.List;

public class RangeIterator implements Iterator<String> {

	private int index1;

	private int index2;

	private int counter;

	private int size;

	private List<String> elems;

	public RangeIterator(List<String> lines, int index1, int index2) {
		if (index1 > index2) {
			this.index1 = index2;
			this.index2 = index1;
		} else {

			this.index1 = index1;
			this.index2 = index2;
		}

		this.elems = lines;
		this.counter = 0;
		this.size = this.index2 - this.index1;
	}

	@Override
	public boolean hasNext() {
		if (index1 < 0) {
			return false;
		} else if (index2 >= elems.size()) {
			return false;
		} else if (counter == size) {
			return false;
		}

		return true;
	}

	@Override
	public String next() {
		return elems.get(index1 + counter++);
	}

}
