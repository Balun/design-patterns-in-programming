package hr.fer.zemris.ooup.lab3.gui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import hr.fer.zemris.ooup.lab3.gui.editor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.gui.editor.util.LocationRange;
import hr.fer.zemris.ooup.lab3.gui.editor.util.Plugin;
import hr.fer.zemris.ooup.lab3.gui.editor.util.UndoManager;

public class ActionProvider {

	public static final String PLUGINS = "hr.fer.zemris.ooup.lab3.gui.editor.plugins.";

	private static Action moveLeftAction;

	private static Action moveRightAction;

	private static Action moveUpAction;

	private static Action moveDownAction;

	private static Action deleteBeforeAction;

	private static Action deleteAfterAction;

	private static Action openAction;

	private static Action saveAction;

	private static Action exitAction;

	private static Action copyAction;

	private static Action pushAction;

	private static Action pasteAction;

	private static Action deletePasteAction;

	private static Action clearAction;

	private static Action moveToStart;

	private static Action moveToEnd;

	private static Action rockAction;

	private ActionProvider() {
	}

	public static Action getUpAction(TextEditorModel model) {
		if (moveUpAction == null) {
			moveUpAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.moveCursorUp();
				}
			};

			moveUpAction.putValue(Action.NAME, "Move cursor up");
			moveUpAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("UP"));
			moveUpAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_UP);
		}

		return moveUpAction;
	}

	public static Action getDownAction(TextEditorModel model) {
		if (moveDownAction == null) {
			moveDownAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.moveCursorDown();
				}
			};

			moveDownAction.putValue(Action.NAME, "Move cursor down");
			moveDownAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("DOWN"));
			moveDownAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_DOWN);
		}

		return moveDownAction;
	}

	public static Action getLeftAction(TextEditorModel model) {
		if (moveLeftAction == null) {
			moveLeftAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.moveCursorLeft();
				}
			};

			moveLeftAction.putValue(Action.NAME, "Move cursor left");
			moveLeftAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("LEFT"));
			moveLeftAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_LEFT);
		}

		return moveLeftAction;
	}

	public static Action getRightAction(TextEditorModel model) {
		if (moveRightAction == null) {
			moveRightAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.moveCursorRight();
				}
			};

			moveRightAction.putValue(Action.NAME, "Move cursor right");
			moveRightAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("RIGHT"));
			moveRightAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_RIGHT);
		}

		return moveRightAction;
	}

	public static Action getDeleteAfterAction(TextEditorModel model) {
		if (deleteAfterAction == null) {
			deleteAfterAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.deleteAfter();
				}
			};

			deleteAfterAction.putValue(Action.NAME, "delete");
			deleteAfterAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("DELETE"));
			deleteAfterAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_DELETE);
		}

		return deleteAfterAction;
	}

	public static Action getDeleteBeforeAction(TextEditorModel model) {
		if (deleteBeforeAction == null) {
			deleteBeforeAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.deleteBefore();
				}
			};

			deleteBeforeAction.putValue(Action.NAME, "backspace");
			deleteBeforeAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("BACK_SPACE"));
			deleteBeforeAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_BACK_SPACE);
		}

		return deleteBeforeAction;
	}

	public static Action getOpenAction(TextEditorModel model) {
		if (openAction == null) {
			openAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JFileChooser chooser = new JFileChooser();
					chooser.showOpenDialog(chooser);

					File selected = chooser.getSelectedFile();
					if (selected != null) {
						StringBuilder builder = new StringBuilder();
						try {
							for (String line : Files.readAllLines(selected.toPath())) {
								builder.append(line + "\n");
							}

							model.setText(builder.toString());
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			};

			openAction.putValue(Action.NAME, "Open");
			openAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
			openAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		}

		return openAction;
	}

	public static Action getSaveAction(TextEditorModel model) {
		if (saveAction == null) {
			saveAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JFileChooser chooser = new JFileChooser();
					chooser.setDialogTitle("Save");
					chooser.showOpenDialog(chooser);

					File selected = chooser.getSelectedFile();
					if (selected != null) {
						try {
							if (!selected.exists()) {
								Files.createFile(selected.toPath());
							}

							Files.write(selected.toPath(), model);
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			};

			saveAction.putValue(Action.NAME, "Save");
			saveAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
			saveAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		}

		return saveAction;
	}

	public static Action getExitAction(TextEditorModel model) {
		if (exitAction == null) {
			exitAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			};

			exitAction.putValue(Action.NAME, "Exit");
			exitAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
			exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_Q);
		}

		return exitAction;
	}

	public static Action getNewPluginAction(TextEditorModel model, String file) {
		try {
			Class<Plugin> pluginClass = (Class<Plugin>) Class.forName(PLUGINS + file);
			Plugin plugin = pluginClass.newInstance();

			Action action = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					// TODO makni ovaj cull kad središ stack
					plugin.execute(model, UndoManager.getInstance(), null);
				}
			};

			action.putValue(Action.NAME, plugin.getName());
			action.putValue(Action.SHORT_DESCRIPTION, plugin.getDescription());

			return action;

		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			JOptionPane.showMessageDialog(null, "Could not load plugin " + file);
		}

		return null;
	}

	public static Action getCopyAction(TextEditorModel model) {
		if (copyAction == null) {
			copyAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					LocationRange range = model.getSelectionRange();
					StringBuilder builder = new StringBuilder();
					Iterator<String> it = model.linesRange(range.getStart().getLine(), range.getEnd().getLine());

					if (range.getStart().getLine() == range.getEnd().getLine() && it.hasNext()) {
						builder.append(it.next().substring(range.getStart().getRow(), range.getEnd().getRow()));

					} else {
						int i = range.getStart().getLine();
						while (it.hasNext()) {
							if (i == range.getStart().getLine()) {
								builder.append(it.next().substring(range.getStart().getRow()));

							} else if (i == range.getEnd().getLine()) {
								builder.append(it.next().substring(0, range.getEnd().getRow()));
							}

							builder.append(it.next() + "\n");
							i++;
						}
					}

					model.getStack().pushText(builder.toString());
				}
			};

			copyAction.putValue(Action.NAME, "Copy");
			copyAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
			copyAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		}

		return copyAction;
	}

	public static Action getPushAction(TextEditorModel model) {
		if (pushAction == null) {
			pushAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					LocationRange range = model.getSelectionRange();

					Iterator<String> it = model.linesRange(range.getStart().getLine(), range.getStart().getLine());
					StringBuilder builder = new StringBuilder();
					StringBuilder remaining = new StringBuilder();

					if (range.getStart().getLine() == range.getEnd().getLine() && it.hasNext()) {
						String tmp = it.next();
						builder.append(tmp.substring(range.getStart().getRow(), range.getEnd().getRow()));
						remaining.append(
								tmp.substring(0, range.getStart().getRow()) + tmp.substring(range.getEnd().getRow()));

					} else {
						int i = range.getStart().getLine();
						while (it.hasNext()) {
							String tmp = it.next();

							if (i == range.getStart().getLine()) {
								builder.append(tmp.substring(range.getStart().getRow()));
								remaining.append(tmp.substring(0, range.getStart().getRow()) + "\n");

							} else if (i == range.getEnd().getLine()) {
								builder.append(tmp.substring(0, range.getEnd().getRow()));
								remaining.append(tmp.substring(range.getEnd().getRow()) + "\n");
							}

							builder.append(tmp + "\n");
							i++;
						}
					}

					model.getStack().pushText(builder.toString());
					model.setText(remaining.toString());
				}
			};

			pushAction.putValue(Action.NAME, "Cut");
			pushAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
			pushAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_X);
		}

		return pushAction;
	}

	public static Action getPasteAction(TextEditorModel model) {
		if (pasteAction == null) {
			pasteAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.insertString(model.getStack().peekText());
				}
			};

			pasteAction.putValue(Action.NAME, "Paste");
			pasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
			pasteAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		}

		return pasteAction;
	}

	public static Action getDeletePasteAction(TextEditorModel model) {
		if (deletePasteAction == null) {
			deletePasteAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.insertString(model.getStack().popText());
				}
			};

			deletePasteAction.putValue(Action.NAME, "Paste and take");
			deletePasteAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift V"));
			deletePasteAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_V);
		}

		return deletePasteAction;
	}

	public static Action getClearAction(TextEditorModel model) {
		if (clearAction == null) {
			clearAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.setText("");
				}
			};

			clearAction.putValue(Action.NAME, "Clear document");
			clearAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control A DELETE"));
		}

		return clearAction;
	}

	public static Action getMoveToStartAction(TextEditorModel model) {
		if (moveToStart == null) {
			moveToStart = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.moveCursorToStart();
				}
			};

			moveToStart.putValue(Action.NAME, "Move to document start");
		}

		return moveToStart;
	}

	public static Action getMoveToEndAction(TextEditorModel model) {
		if (moveToEnd == null) {
			moveToEnd = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					model.moveCursorToEnd();
				}
			};

			moveToEnd.putValue(Action.NAME, "Move to document end");
		}

		return moveToEnd;
	}

	public static Action getRockAction(TextEditorModel model) {
		if (rockAction == null) {
			rockAction = new AbstractAction() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JOptionPane.showMessageDialog(null, "ROKAAAAAAAJJJJ!!!");
				}
			};

			rockAction.putValue(Action.NAME, "Rock 'n' Roll");
		}

		return rockAction;
	}

}
