package hr.fer.zemris.ooup.lab3.gui.observers;

public interface TextObserver {
	
	void updateText();

}
