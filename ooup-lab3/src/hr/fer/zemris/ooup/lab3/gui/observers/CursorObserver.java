package hr.fer.zemris.ooup.lab3.gui.observers;

import hr.fer.zemris.ooup.lab3.gui.editor.util.Location;

public interface CursorObserver {
	
	void updateCursorLocation(Location loc);

}
