package hr.fer.zemris.ooup.lab3.gui.editor.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import hr.fer.zemris.ooup.lab3.gui.observers.ClipboardObserver;

public class ClipboardStack {

	private List<String> stack;

	private List<ClipboardObserver> observers;

	public ClipboardStack() {
		this.stack = new ArrayList<>();
		this.observers = new LinkedList<>();
	}

	public void pushText(String text) {
		this.stack.add(text);
		publish();
	}

	public String popText() {
		String elem = this.stack.get(this.stack.size() - 1);
		this.stack.remove(this.stack.size() - 1);
		publish();
		return elem;
	}

	public String peekText() {
		publish();
		return this.stack.get(this.stack.size() - 1);
	}

	public boolean isEmpty() {
		return this.stack.isEmpty();
	}

	public void clearStack() {
		this.stack.clear();
	}

	public void addClipboardObserver(ClipboardObserver observer) {
		observers.add(observer);
	}

	public void removeClipboardObserver(ClipboardObserver observer) {
		observers.remove(observer);
	}

	private void publish() {
		observers.forEach(o -> o.updateClipboard());
	}

}
