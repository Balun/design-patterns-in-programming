package hr.fer.zemris.ooup.lab3.gui.editor.util;

import hr.fer.zemris.ooup.lab3.gui.editor.TextEditorModel;

public interface Plugin {

	String getName();

	String getDescription();

	public void execute(TextEditorModel model, UndoManager manager, ClipboardStack stack);
}
