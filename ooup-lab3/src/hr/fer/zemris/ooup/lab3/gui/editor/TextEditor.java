package hr.fer.zemris.ooup.lab3.gui.editor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JComponent;
import javax.swing.SwingWorker;

import hr.fer.zemris.ooup.lab3.gui.editor.util.Location;
import hr.fer.zemris.ooup.lab3.gui.editor.util.LocationRange;
import hr.fer.zemris.ooup.lab3.gui.observers.ClipboardObserver;
import hr.fer.zemris.ooup.lab3.gui.observers.CursorObserver;
import hr.fer.zemris.ooup.lab3.gui.observers.TextObserver;

public class TextEditor extends JComponent implements CursorObserver, TextObserver, ClipboardObserver {

	private TextEditorModel model;

	private Location cursor;

	private boolean cursorVisible;

	public TextEditor(TextEditorModel model) {
		this.model = model;

		model.addCursorObserver(this);
		model.addTextObserver(this);
		model.registerClipboardObsever(this);

		this.cursor = model.getCursor();
		this.cursorVisible = true;
		setOpaque(true);
		setBackground(Color.WHITE);
		startCursor();

		setKeyListener();
		setFocusable(true);
	}

	private void setKeyListener() {
		addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {
				char c = e.getKeyChar();
				
				if(Character.isLetterOrDigit(c) || ((int)c == 10)){
					model.insert(c);
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
			}
		});
	}

	public TextEditorModel getModel() {
		return model;
	}

	@Override
	protected synchronized void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.BLACK);
		g2.fillRect(0, 0, getWidth(), getHeight());

		drawText(g2);
	}

	private void drawText(Graphics2D g) {
		g.setColor(Color.GREEN);
		g.setFont(new Font(Font.DIALOG, Font.PLAIN, 17));

		int offset = g.getFontMetrics().getHeight();

		for (String line : model) {
			g.drawString(line, 0, offset);
			offset += g.getFontMetrics().getHeight();
		}

		if (cursorVisible) {
			drawCursor(g);
		}
	}

	private void drawCursor(Graphics2D g) {
		int width = calculateCaretPosition(g);
		g.drawString("|", width, (cursor.getLine() + 1) * g.getFontMetrics().getHeight());
	}

	private int calculateCaretPosition(Graphics2D g) {
		int pos = 0;
		String tmp = model.linesRange(cursor.getLine(), cursor.getLine()).next().substring(0, cursor.getRow());
		pos = g.getFontMetrics().stringWidth(tmp);
		return pos;
	}

	@Override
	public void updateCursorLocation(Location loc) {
		cursor = loc;
		repaint();
	}

	private void startCursor() {
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

			@Override
			protected Void doInBackground() throws Exception {
				while (true) {
					Thread.sleep(500);
					cursorVisible = !cursorVisible;
					TextEditor.this.repaint();
				}
			}
		};

		worker.execute();
	}

	@Override
	public void updateText() {
		cursor = model.getCursor();
		repaint();
	}

	@Override
	public void updateClipboard() {
		repaint();
	}
}
