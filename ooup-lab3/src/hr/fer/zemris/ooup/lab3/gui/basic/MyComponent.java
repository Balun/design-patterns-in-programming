package hr.fer.zemris.ooup.lab3.gui.basic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.geom.Line2D;

import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

public class MyComponent extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4024497369610179179L;

	private JFrame parent;

	private AbstractAction closeAction;

	public MyComponent(JFrame parent) {
		super();
		this.parent = parent;

		closeAction = new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				parent.dispose();
			}
		};

		getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "close");
		getActionMap().put("close", closeAction);
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;

		g2.setColor(Color.RED);

		Line2D line1 = new Line2D.Double(100, 100, 100, 200);
		g2.draw(line1);

		Line2D line2 = new Line2D.Double(150, 150, 300, 150);
		g2.draw(line2);

		g2.setColor(Color.BLACK);
		g2.drawString("Danas je lijep i sunčan dan!", 200, 200);
		g2.drawString("I zato izlazim van!", 200, 200 + g2.getFontMetrics().getHeight());
	}
	
}
