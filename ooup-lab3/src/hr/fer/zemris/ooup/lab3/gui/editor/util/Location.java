package hr.fer.zemris.ooup.lab3.gui.editor.util;

public class Location {
	
	private int line;
	
	private int row;

	public Location(int line, int row) {
		super();
		this.line = line;
		this.row = row;
	}

	public int getLine() {
		return line;
	}

	public int getRow() {
		return row;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + line;
		result = prime * result + row;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (line != other.line)
			return false;
		if (row != other.row)
			return false;
		return true;
	}
	
	

}
