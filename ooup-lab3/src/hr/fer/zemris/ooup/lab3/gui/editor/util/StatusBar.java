package hr.fer.zemris.ooup.lab3.gui.editor.util;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import hr.fer.zemris.ooup.lab3.gui.editor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.gui.observers.CursorObserver;
import hr.fer.zemris.ooup.lab3.gui.observers.TextObserver;

public class StatusBar extends JPanel implements CursorObserver, TextObserver {

	private JLabel cursorLabel;

	private JLabel lengthLabel;

	private TextEditorModel model;

	public StatusBar(TextEditorModel model) {
		this.cursorLabel = new JLabel("ln: " + model.getCursor().getLine() + " | row: " + model.getCursor().getRow());
		this.lengthLabel = new JLabel("length: " + model.size());
		this.model = model;

		setLayout(new GridLayout(0, 2));
		add(cursorLabel);
		add(lengthLabel);
	}

	@Override
	public void updateText() {
		lengthLabel.setText("length: " + model.size());
	}

	@Override
	public void updateCursorLocation(Location loc) {
		this.cursorLabel.setText("ln: " + loc.getLine() + " | row:" + loc.getRow());
	}

}
