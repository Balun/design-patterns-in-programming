package hr.fer.zemris.ooup.lab3.gui.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.ooup.lab3.gui.editor.util.ClipboardStack;
import hr.fer.zemris.ooup.lab3.gui.editor.util.Location;
import hr.fer.zemris.ooup.lab3.gui.editor.util.LocationRange;
import hr.fer.zemris.ooup.lab3.gui.editor.util.RangeIterator;
import hr.fer.zemris.ooup.lab3.gui.observers.ClipboardObserver;
import hr.fer.zemris.ooup.lab3.gui.observers.CursorObserver;
import hr.fer.zemris.ooup.lab3.gui.observers.TextObserver;

public class TextEditorModel implements Iterable<String> {

	private List<String> lines;

	private List<CursorObserver> cursorObservers;

	private List<TextObserver> textObservers;

	private LocationRange selectionRange;

	private Location cursorLocation;

	private ClipboardStack stack;

	public TextEditorModel(String text) {
		this.lines = new ArrayList<>(Arrays.asList(text.split("\\n")));
		this.cursorLocation = new Location(0, 0);
		this.selectionRange = new LocationRange(new Location(0, 0), new Location(0, 0));
		this.cursorObservers = new LinkedList<>();
		this.textObservers = new LinkedList<>();
		this.stack = new ClipboardStack();
	}

	public TextEditorModel() {
		this("");
	}

	public void setText(String text) {
		this.lines = new ArrayList<>(Arrays.asList(text.split("\\n")));
		publishTextChange();
		Location loc = new Location(0, 0);
		publishCursorLocation(loc);
	}

	public ClipboardStack getStack() {
		return this.stack;
	}

	public Location getCursor() {
		return cursorLocation;
	}

	public int size() {
		return lines.size();
	}

	public void registerClipboardObsever(ClipboardObserver observer) {
		this.stack.addClipboardObserver(observer);
	}

	public void deleteClipboardObserver(ClipboardObserver observer) {
		this.stack.removeClipboardObserver(observer);
	}

	public void addCursorObserver(CursorObserver observer) {
		cursorObservers.add(observer);
	}

	public void removeCursorObserver(CursorObserver observer) {
		if (cursorObservers.contains(observer)) {
			cursorObservers.remove(cursorObservers);
		}
	}

	private void publishCursorLocation(Location loc) {
		for (CursorObserver observer : cursorObservers) {
			observer.updateCursorLocation(loc);
			cursorLocation = loc;
		}
	}

	public void moveCursorLeft() {
		if (cursorLocation.getRow() > 0) {
			Location newLocation = new Location(cursorLocation.getLine(), cursorLocation.getRow() - 1);
			publishCursorLocation(newLocation);

		} else if (cursorLocation.getRow() == 0 && cursorLocation.getLine() > 0) {
			Location newLocation = new Location(cursorLocation.getLine() - 1,
					lines.get(cursorLocation.getLine() - 1).length());
			publishCursorLocation(newLocation);
		}
	}

	public void moveCursorUp() {
		if (cursorLocation.getLine() > 0) {
			if (cursorLocation.getRow() > lines.get(cursorLocation.getLine() - 1).length()) {
				Location loc = new Location(cursorLocation.getLine() - 1,
						lines.get(cursorLocation.getLine() - 1).length());
				publishCursorLocation(loc);

			} else {
				Location loc = new Location(cursorLocation.getLine() - 1, cursorLocation.getRow());
				publishCursorLocation(loc);
			}
		}

	}

	public void moveCursorRight() {
		if (cursorLocation.getRow() < lines.get(cursorLocation.getLine()).length()) {
			Location loc = new Location(cursorLocation.getLine(), cursorLocation.getRow() + 1);
			publishCursorLocation(loc);

		} else if (cursorLocation.getRow() == lines.get(cursorLocation.getLine()).length()
				&& cursorLocation.getLine() < lines.size()) {

			Location loc = new Location(cursorLocation.getLine() + 1, 0);
			publishCursorLocation(loc);
		}
	}

	public void moveCursorDown() {
		if (cursorLocation.getLine() < lines.size() - 1) {
			if (cursorLocation.getRow() > lines.get(cursorLocation.getLine() + 1).length()) {
				Location loc = new Location(cursorLocation.getLine() + 1,
						lines.get(cursorLocation.getLine() + 1).length());
				publishCursorLocation(loc);
			} else {

				Location loc = new Location(cursorLocation.getLine() + 1, cursorLocation.getRow());
				publishCursorLocation(loc);
			}
		}
	}

	public void moveCursorToStart() {
		Location loc = new Location(0, 0);
		publishCursorLocation(loc);
	}

	public void moveCursorToEnd() {
		Location loc = new Location(lines.size() - 1, lines.get(lines.size() - 1).length());
		publishCursorLocation(loc);
	}

	public void addTextObserver(TextObserver observer) {
		this.textObservers.add(observer);
	}

	public void removeTextObserver(TextObserver observer) {
		this.textObservers.remove(observer);
	}

	private void publishTextChange() {
		for (TextObserver observer : textObservers) {
			observer.updateText();
		}
	}

	public void deleteBefore() {
		String tmp = lines.get(cursorLocation.getLine());

		if (cursorLocation.getRow() > 0) {
			String newString = tmp.substring(0, cursorLocation.getRow() - 1) + tmp.substring(cursorLocation.getRow());
			lines.set(cursorLocation.getLine(), newString);
			moveCursorLeft();
			publishTextChange();

		} else if (cursorLocation.getLine() > 0) {
			lines.remove(cursorLocation.getLine());
			int line = cursorLocation.getLine() - 1;
			String upperString = lines.get(line);
			lines.set(line, upperString.replaceAll("\n", "") + tmp);
			cursorLocation = new Location(line, upperString.length());
			publishTextChange();
		}
	}

	public void deleteAfter() {
		String tmp = lines.get(cursorLocation.getLine());

		if (cursorLocation.getRow() < tmp.length()) {
			String newString = tmp.substring(0, cursorLocation.getRow()) + tmp.substring(cursorLocation.getRow() + 1);
			lines.set(cursorLocation.getLine(), newString);
			publishTextChange();

		} else if (cursorLocation.getLine() < lines.size() - 1) {
			String downLine = lines.get(cursorLocation.getLine() + 1);
			lines.remove(cursorLocation.getLine() + 1);
			lines.set(cursorLocation.getLine(), tmp + downLine);
			publishTextChange();
		}
	}

	public LocationRange getSelectionRange() {
		return this.selectionRange;
	}

	public void setSelectionRange(LocationRange range) {
		this.selectionRange = range;
		publishTextChange();
	}

	public void insert(char c) {
		String line = lines.get(cursorLocation.getLine());
		if (((int) c) == 10) {
			String first = line.substring(0, cursorLocation.getRow());
			String second = line.substring(cursorLocation.getRow());

			lines.set(cursorLocation.getLine(), first);
			lines.add(cursorLocation.getLine() + 1, second);

			Location loc = new Location(cursorLocation.getLine() + 1, 0);
			publishCursorLocation(loc);

		} else {
			line = line.substring(0, cursorLocation.getRow()) + c + line.substring(cursorLocation.getRow());
			lines.set(cursorLocation.getLine(), line);
			moveCursorRight();
		}

		publishTextChange();
	}

	public void insertString(String str) {
		String line = lines.get(cursorLocation.getLine());

		if (str.contains("\n")) {
			for (String chunk : str.split("\n")) {
				line = line.substring(0, cursorLocation.getRow()) + chunk + line.substring(cursorLocation.getRow());
				lines.set(cursorLocation.getLine(), line);
				Location loc = new Location(cursorLocation.getLine(), cursorLocation.getRow() + chunk.length());
				publishTextChange();
				publishCursorLocation(loc);
				insert('\n');
			}

		} else {
			line = line.substring(0, cursorLocation.getRow()) + str + line.substring(cursorLocation.getRow());
			lines.set(cursorLocation.getLine(), line);
			Location loc = new Location(cursorLocation.getLine(), cursorLocation.getRow() + str.length());
			publishTextChange();
			publishCursorLocation(loc);
		}
	}

	public Iterator<String> allLines() {
		return lines.iterator();
	}

	public Iterator<String> linesRange(int index1, int index2) {
		return new RangeIterator(lines, index1, index2);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (String line : lines) {
			builder.append(line + "\n");
		}

		return builder.toString();
	}

	@Override
	public Iterator<String> iterator() {
		return allLines();
	}

}
