package hr.fer.zemris.ooup.lab3.gui.editor.util;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.ImageIcon;

public class IconProvider {

	public static ImageIcon getIcon(String name) throws FileNotFoundException {
		String path = "res/" + name + ".png";

		InputStream input = new FileInputStream(path);

		if (input == null) {
			System.err.println("Error in loading icons");
			return null;
		}

		byte[] bytes = new byte[4096];
		try (InputStream is = new BufferedInputStream(input)) {
			is.read(bytes);
		} catch (IOException e1) {
			System.err.println("Unable to read icon..");
		} finally {
			try {
				input.close();
			} catch (IOException e) {
			}
		}

		return new ImageIcon(bytes);
	}

}
