package hr.fer.zemris.ooup.lab3.gui.editor.util;

import java.util.Stack;

import hr.fer.zemris.ooup.lab3.gui.actions.EditAction;
import hr.fer.zemris.ooup.lab3.gui.observers.ClipboardObserver;

public class UndoManager implements ClipboardObserver {

	private static UndoManager instance;

	private Stack<EditAction> undoStack;

	private Stack<EditAction> redoStack;

	private UndoManager() {
		this.undoStack = new Stack<>();
		this.redoStack = new Stack<>();
	}

	public void undo() {
		if (!undoStack.isEmpty()) {
			EditAction action = undoStack.pop();
			redoStack.push(action);
			action.executeUndo();
		}
	}

	public void push(EditAction action) {
		redoStack.clear();
		undoStack.push(action);
	}

	public static UndoManager getInstance() {
		if (instance == null) {
			instance = new UndoManager();
		}

		return instance;
	}

	@Override
	public void updateClipboard() {
		// TODO Auto-generated method stub

	}

}
