package hr.fer.zemris.ooup.lab3.gui.basic;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class BasicFrame extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BasicFrame() {
		// TODO Auto-generated constructor stub
		initGUI();
	}

	private void initGUI() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setLocation(100, 100);
		setSize(500, 500);
		
		JComponent component = new MyComponent(this);
		add(component);
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new BasicFrame().setVisible(true));
	}
}
