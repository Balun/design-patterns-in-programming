package hr.fer.zemris.ooup.lab3.gui.actions;

public interface EditAction {

	public void executeDo();

	public void executeUndo();

}
