package hr.fer.zemris.ooup.lab3.gui.editor.plugins;

import javax.swing.JOptionPane;

import hr.fer.zemris.ooup.lab3.gui.editor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.gui.editor.util.ClipboardStack;
import hr.fer.zemris.ooup.lab3.gui.editor.util.Plugin;
import hr.fer.zemris.ooup.lab3.gui.editor.util.UndoManager;

public class Statistic implements Plugin {

	private static final String NAME = "Statistics";

	private static final String DESCRIPTION = "Calculates the statistics of the document.";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public void execute(TextEditorModel model, UndoManager manager, ClipboardStack stack) {
		int lines = model.size();
		int words = 0;
		int letters = 0;

		for (String line : model) {
			words += line.split(" ").length;
			letters += line.replaceAll(" ", "").length();
		}

		JOptionPane.showMessageDialog(null,
				String.format("Broj redaka: %d\nBroj riječi: %d\nBroj slova: %d", lines, words, letters));
	}
}
