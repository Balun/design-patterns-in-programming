package hr.fer.zemris.ooup.lab3.gui.editor.demo;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import hr.fer.zemris.ooup.lab3.gui.actions.ActionProvider;
import hr.fer.zemris.ooup.lab3.gui.editor.TextEditor;
import hr.fer.zemris.ooup.lab3.gui.editor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.gui.editor.util.IconProvider;
import hr.fer.zemris.ooup.lab3.gui.editor.util.StatusBar;

public class JNano extends JFrame {

	public static final String PLUGIN_PATH = "src/hr/fer/zemris/ooup/lab3/gui/editor/plugins";

	private TextEditorModel model;

	public JNano(String title) {
		setTitle(title);
		setLocation(0, 0);
		setSize(1000, 1000);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setLayout(new BorderLayout());

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// nothing
		}

		initGUI();
	}

	private void initGUI() {
		this.model = new TextEditorModel(createText());

		try {
			createToolbar();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		TextEditor editor = new TextEditor(model);
		editor.setRequestFocusEnabled(true);

		JPanel mainPanel = new JPanel(new BorderLayout());
		mainPanel.add(editor, BorderLayout.CENTER);

		mainPanel.add(editor, BorderLayout.CENTER);
		add(mainPanel, BorderLayout.CENTER);

		createMenus();
		createStatusBar();
	}

	private void loadPlugins(JMenu pluginMenu) {
		Path plugins = Paths.get(PLUGIN_PATH);
		if (Files.exists(plugins)) {
			for (String file : plugins.toFile().list()) {
				file = file.substring(0, file.lastIndexOf('.'));
				Action action = ActionProvider.getNewPluginAction(model, file);
				pluginMenu.add(new JMenuItem(action));
			}
		}

	}

	private void createStatusBar() {
		StatusBar bar = new StatusBar(model);
		model.addCursorObserver(bar);
		model.addTextObserver(bar);

		add(bar, BorderLayout.SOUTH);
	}

	private void createToolbar() throws FileNotFoundException {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setRequestFocusEnabled(false);
		getContentPane().add(toolBar, BorderLayout.NORTH);

		JButton open = new JButton(ActionProvider.getOpenAction(model));
		open.setIcon(IconProvider.getIcon("open"));
		open.setToolTipText("Open document");
		open.setText("");
		open.setFocusable(false);
		toolBar.add(open);

		JButton save = new JButton(ActionProvider.getSaveAction(model));
		save.setIcon(IconProvider.getIcon("save"));
		save.setToolTipText("Save document");
		save.setText("");
		save.setFocusable(false);
		toolBar.add(save);

		JButton exit = new JButton(ActionProvider.getExitAction(model));
		exit.setIcon(IconProvider.getIcon("exit"));
		exit.setToolTipText("Exit the editor");
		exit.setText("");
		exit.setFocusable(false);
		toolBar.add(exit);

		toolBar.addSeparator();

		JButton undo = new JButton();
		// TODO dodati akciju
		undo.setIcon(IconProvider.getIcon("undo"));
		undo.setToolTipText("Undo action");
		undo.setFocusable(false);
		toolBar.add(undo);

		JButton redo = new JButton();
		// TODO dodati akciju
		redo.setIcon(IconProvider.getIcon("redo"));
		redo.setFocusable(false);
		redo.setToolTipText("Redo action");
		toolBar.add(redo);

		toolBar.addSeparator();

		JButton cut = new JButton(ActionProvider.getPushAction(model));
		cut.setIcon(IconProvider.getIcon("cut"));
		cut.setToolTipText("Cut selection");
		cut.setText("");
		cut.setFocusable(false);
		toolBar.add(cut);

		JButton copy = new JButton(ActionProvider.getCopyAction(model));
		copy.setIcon(IconProvider.getIcon("copy"));
		copy.setToolTipText("Copy selection");
		copy.setText("");
		copy.setFocusable(false);
		toolBar.add(copy);

		JButton paste = new JButton(ActionProvider.getPasteAction(model));
		paste.setIcon(IconProvider.getIcon("paste"));
		paste.setToolTipText("Paste selection");
		paste.setText("");
		paste.setFocusable(false);
		toolBar.add(paste);

		JButton paste2 = new JButton(ActionProvider.getDeletePasteAction(model));
		paste2.setIcon(IconProvider.getIcon("paste2"));
		paste2.setToolTipText("Paste and take");
		paste2.setText("");
		paste2.setFocusable(false);
		toolBar.add(paste2);

		toolBar.addSeparator();

		JButton rock = new JButton(ActionProvider.getRockAction(model));
		rock.setIcon(IconProvider.getIcon("rock"));
		rock.setToolTipText("Rock 'n' roll!");
		rock.setText("");
		rock.setFocusable(false);
		toolBar.add(rock);
		
		toolBar.addSeparator();
	}

	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		fileMenu.add(new JMenuItem(ActionProvider.getOpenAction(model)));
		fileMenu.add(new JMenuItem(ActionProvider.getSaveAction(model)));
		fileMenu.add(new JMenuItem(ActionProvider.getExitAction(model)));

		JMenu editMenu = new JMenu("Edit");
		menuBar.add(editMenu);

		editMenu.add(new JMenuItem(ActionProvider.getDeleteBeforeAction(model)));
		editMenu.add(new JMenuItem(ActionProvider.getDeleteAfterAction(model)));
		editMenu.add(new JMenuItem(ActionProvider.getPushAction(model)));
		editMenu.add(new JMenuItem(ActionProvider.getCopyAction(model)));
		editMenu.add(new JMenuItem(ActionProvider.getPasteAction(model)));
		editMenu.add(new JMenuItem(ActionProvider.getDeletePasteAction(model)));
		editMenu.add(new JMenuItem(ActionProvider.getClearAction(model)));

		JMenu pluginMenu = new JMenu("Plugins");
		menuBar.add(pluginMenu);

		loadPlugins(pluginMenu);

		JMenu movementMenu = new JMenu("Move");
		menuBar.add(movementMenu);

		movementMenu.add(new JMenuItem(ActionProvider.getDownAction(model)));
		movementMenu.add(new JMenuItem(ActionProvider.getLeftAction(model)));
		movementMenu.add(new JMenuItem(ActionProvider.getRightAction(model)));
		movementMenu.add(new JMenuItem(ActionProvider.getUpAction(model)));
		movementMenu.add(new JMenuItem(ActionProvider.getMoveToStartAction(model)));
		movementMenu.add(new JMenuItem(ActionProvider.getMoveToEndAction(model)));

		setJMenuBar(menuBar);
	}

	private String createText() {
		StringBuilder builder = new StringBuilder();
		builder.append("Bok Matej!\n");
		builder.append("Danas je lijep i sunčan dan.\n");
		builder.append("Zašto sjediš za računalom?\n");
		builder.append("Rađe otiđi van!\n");

		return builder.toString();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new JNano("JNano").setVisible(true));
	}
}
