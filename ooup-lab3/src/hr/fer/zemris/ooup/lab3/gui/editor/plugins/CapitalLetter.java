package hr.fer.zemris.ooup.lab3.gui.editor.plugins;

import hr.fer.zemris.ooup.lab3.gui.editor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.gui.editor.util.ClipboardStack;
import hr.fer.zemris.ooup.lab3.gui.editor.util.Plugin;
import hr.fer.zemris.ooup.lab3.gui.editor.util.UndoManager;

public class CapitalLetter implements Plugin {

	private static final String NAME = "Capital letter";

	private static final String DESCRIPTION = "Capitalizes all the words in document";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public String getDescription() {
		return DESCRIPTION;
	}

	@Override
	public void execute(TextEditorModel model, UndoManager manager, ClipboardStack stack) {
		StringBuilder builder = new StringBuilder();

		for (String line : model) {
			for (String word : line.split(" ")) {
				builder.append(Character.toUpperCase(word.charAt(0)) + word.substring(1) + " ");
			}

			builder.replace(builder.length() - 1, builder.length(), "\n");
		}

		model.setText(builder.toString());
	}

}
