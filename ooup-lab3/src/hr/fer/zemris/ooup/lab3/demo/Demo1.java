package hr.fer.zemris.ooup.lab3.demo;

import java.util.Scanner;

import hr.fer.zemris.ooup.lab3.model.Animal;
import hr.fer.zemris.ooup.lab3.model.AnimalFactory;

public class Demo1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		while (true) {
			System.out.println("Unesite vrstu životinje:");
			String animalKind = sc.nextLine();

			if (animalKind.equalsIgnoreCase("done")) {
				System.out.println("doviđenja!");
				break;
			}

			System.out.println("Unesite ime životinje:");
			String name = sc.nextLine();

			try {
				Animal animal = AnimalFactory.newInstance(animalKind, name);
				animal.animalPrintGreeting();
				animal.animalPrintMenu();
				System.out.println();

			} catch (IllegalArgumentException e) {
				System.err.println(e.getMessage());
				System.out.println();
				continue;
			}
		}

		sc.close();
	}

}
