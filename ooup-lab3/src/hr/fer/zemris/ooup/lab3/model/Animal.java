package hr.fer.zemris.ooup.lab3.model;

public abstract class Animal {
	
	protected String animalName;
	
	public Animal(String animalName) {
		this.animalName = animalName;
	}
	
	public abstract String name();
	
	public abstract String greet();
	
	public abstract String menu();

	public void animalPrintGreeting() {
		System.out.println("Ljubimac " + name() + " pozdravlja: " + greet() + "!");
	}
	
	public void animalPrintMenu() {
		System.out.println("Ljubimac " + name() + " voli " + menu());
	}
}
