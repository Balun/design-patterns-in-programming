package hr.fer.zemris.ooup.lab3.model;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;

import hr.fer.zemris.ooup.lab3.util.LoadStrategy;

/**
 * 
 * @author lumba
 *
 */
public class AnimalFactory {

	/**
	 * 
	 */
	public static final String ANIMAL_PACKAGE = "hr.fer.zemris.ooup.lab3.model.plugins.";

	/**
	 * 
	 */
	private static final ClassLoader LOADER = AnimalFactory.class.getClassLoader();

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	private static final LoadStrategy<Animal> LOAD_FROM_CLASSPATH = (name,
			lib) -> (Class<Animal>) Class.forName(ANIMAL_PACKAGE + name);;

			/**
			 * 
			 */
	private static final LoadStrategy<Animal> LOAD_FROM_JAR = (name, lib) -> loadfFromLib(lib[0], name);

	/**
	 * 
	 * @param animalKind
	 * @param name
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static Animal newInstance(String animalKind, String name) throws IllegalArgumentException {
		return load(animalKind, name, LOAD_FROM_CLASSPATH);
	}

	/**
	 * 
	 * @param animalKind
	 * @param name
	 * @param libPath
	 * @return
	 * @throws IllegalArgumentException
	 */
	public static Animal newInstance(String animalKind, String name, String libPath) throws IllegalArgumentException {
		return load(animalKind, name, LOAD_FROM_JAR, libPath);
	}

	/**
	 * 
	 * @param animalKind
	 * @param name
	 * @param loader
	 * @param lib
	 * @return
	 * @throws IllegalArgumentException
	 */
	private static Animal load(String animalKind, String name, LoadStrategy<Animal> loader, String... lib)
			throws IllegalArgumentException {
		Animal animal = null;

		try {
			Class<Animal> animalClass = null;
			animalClass = loader.load(refactorAnimalType(animalKind), lib);

			Constructor<?> ctr = animalClass.getConstructor(String.class);
			animal = (Animal) ctr.newInstance(name);

		} catch (ClassNotFoundException | NoSuchMethodException e) {
			throw new IllegalArgumentException("Ne postoji razred sa takvim imenom!");

		} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
			// nece se nikad desit
		}

		return animal;
	}

	/**
	 * 
	 * @param animal
	 * @return
	 */
	private static String refactorAnimalType(String animal) {
		animal = animal.toLowerCase();
		animal = animal.replaceFirst(Character.toString(animal.charAt(0)),
				Character.toString(animal.charAt(0)).toUpperCase());

		return animal;
	}

	/**
	 * 
	 * @param path
	 * @param animalKind
	 * @return
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	private static Class<Animal> loadfFromLib(String path, String animalKind) throws ClassNotFoundException {
		try {
			URLClassLoader newClassLoader = new URLClassLoader(new URL[] { new File(path).toURI().toURL() }, LOADER);
			newClassLoader.close();

			return (Class<Animal>) newClassLoader.loadClass(ANIMAL_PACKAGE + animalKind);

		} catch (IOException e) {
			e.printStackTrace();

		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Ne postoji razred sa takvim imenom!");
		}

		return null;
	}

}
