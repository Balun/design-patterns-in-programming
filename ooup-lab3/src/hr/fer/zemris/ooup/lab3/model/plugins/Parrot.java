package hr.fer.zemris.ooup.lab3.model.plugins;

import hr.fer.zemris.ooup.lab3.model.Animal;

public class Parrot extends Animal {

	public Parrot(String name) {
		super(name);
	}

	@Override
	public String name() {
		return animalName;
	}

	@Override
	public String greet() {
		return "Sto mu gromova";
	}

	@Override
	public String menu() {
		return "brazilske orahe";
	}

}
