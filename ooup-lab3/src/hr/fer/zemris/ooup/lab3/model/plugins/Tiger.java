package hr.fer.zemris.ooup.lab3.model.plugins;

import hr.fer.zemris.ooup.lab3.model.Animal;

public class Tiger extends Animal {

	public Tiger(String name) {
		super(name);
	}

	@Override
	public String name() {
		return animalName;
	}

	@Override
	public String greet() {
		return "mijau";
	}

	@Override
	public String menu() {
		return "mlijeku";
	}

}
