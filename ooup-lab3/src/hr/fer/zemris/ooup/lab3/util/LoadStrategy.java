package hr.fer.zemris.ooup.lab3.util;

public interface LoadStrategy<T> {

	Class<T> load(String name, String... jars) throws ClassNotFoundException, IllegalArgumentException;

}
