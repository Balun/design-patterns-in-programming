/*
 * myfactory.h
 *
 *  Created on: May 10, 2017
 *      Author: lumba
 */

#ifndef MYFACTORY_H_
#define MYFACTORY_H_

void* myfactory(char const*, char const*);

#endif /* MYFACTORY_H_ */
