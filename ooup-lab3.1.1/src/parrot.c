/*
 * parrot.c
 *
 *  Created on: May 10, 2017
 *      Author: lumba
 */

#include <stdlib.h>

typedef char const* (*PTRFUN)();

struct Parrot {
	PTRFUN* vtable;
	const char* name;
};

const char *name(void *this) {
	return ((struct Parrot*) this)->name;
}

const char *greet(void *this) {
	return "Sto mu gromova";
}

const char *menu(void *this) {
	return "brazilske orahe";
}

PTRFUN VTable[3] = { name, greet, menu };

void *create(char const *name) {
	struct Parrot *animal = malloc(sizeof(struct Parrot));
	animal->vtable = VTable;
	animal->name = name;
	return animal;
}

