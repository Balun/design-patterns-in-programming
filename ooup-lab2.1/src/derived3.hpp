/*
 * derived3.hpp
 *
 *  Created on: Apr 12, 2017
 *      Author: lumba
 */

#ifndef DERIVED3_HPP_
#define DERIVED3_HPP_

class Derived3 : public Base{

public:
	virtual int solve(){
		return 2;
	}
};


#endif /* DERIVED3_HPP_ */
