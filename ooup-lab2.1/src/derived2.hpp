/*
 * derived2.hpp
 *
 *  Created on: Apr 12, 2017
 *      Author: lumba
 */

#include "base.hpp"

class Derived2: public Base {
public:
	virtual int solve(){
		return 1;
	}
};
