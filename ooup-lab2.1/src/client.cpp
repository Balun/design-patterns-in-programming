/*
 * client.;cpp
 *
 *  Created on: Apr 12, 2017
 *      Author: lumba
 */
#include "client.hpp"
#include <iostream>

using namespace std;

void Client::operate() {
	cout << solver_.solve() << endl;
}

