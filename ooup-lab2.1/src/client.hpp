/*
 * client.hpp
 *
 *  Created on: Apr 12, 2017
 *      Author: lumba
 */

#include "base.hpp"

class Client {

public:
	Client(Base& b) :
			solver_(b) {
	}

	void operate();

private:
	Base& solver_;
};
