/*
 * base.hpp
 *
 *  Created on: Apr 12, 2017
 *      Author: lumba
 */

#ifndef BASE_HPP_
#define BASE_HPP_

class Base {

public:
	virtual ~Base() = default;
	virtual int solve() = 0;
};

#endif /* BASE_HPP_ */
