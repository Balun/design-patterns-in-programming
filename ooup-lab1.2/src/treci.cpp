//============================================================================
// Name        : 2.cpp
// Author      : Matej Balun
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdio.h>

class CoolClass {
public:
	virtual void set(int x) {
		x_ = x;
	}
	virtual int get() {
		return x_;
	}
private:
	int x_;
};

class PlainOldClass {
public:
	void set(int x) {
		x_ = x;
	}
	int get() {
		return x_;
	}
private:
	int x_;
};

int main(int argc, char **argv) {
	printf("memorijsko zauzeće razreda CoolClass je %ld bajtova, a PlainOldClass %ld bajtova\n", sizeof(CoolClass), sizeof(PlainOldClass));

	//objasni razliku u memoriji!!
}
