#include <stdio.h>
#include <iostream>

class B {
public:
	virtual int prva()=0;
	virtual int druga()=0;
};

class D: public B {
public:
	virtual int prva() {
		return 0;
	}
	virtual int druga() {
		return 42;
	}
};

//int main() {
//	B *pb = new D;
//	int* vptr = *(int**) pb;
//
//	printf("prva: %d\n", ( (int (*)()) vptr[0] )());
//	printf("druga: %d\n", ( (int (*)()) vptr[2] )());
//}
